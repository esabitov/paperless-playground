# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( jquery.min.js )
Rails.application.config.assets.precompile += %w( jquery-ui.min.js )
Rails.application.config.assets.precompile += %w( jquery.ui.touch-punch.min.js )
Rails.application.config.assets.precompile += %w( jquery.mCustomScrollbar.concat.min.js )
Rails.application.config.assets.precompile += %w( jquery-ui.min.css )
Rails.application.config.assets.precompile += %w( jquery.mCustomScrollbar.css )
Rails.application.config.assets.precompile += %w( css.css )
Rails.application.config.assets.precompile += %w( global_functions.js )
Rails.application.config.assets.precompile += %w( globals.js )
Rails.application.config.assets.precompile += %w( lightbox.js )
Rails.application.config.assets.precompile += %w( menu.js )
Rails.application.config.assets.precompile += %w( init.js )
Rails.application.config.assets.precompile += %w( glossary.js )
Rails.application.config.assets.precompile += %w( lodash.min.js )
Rails.application.config.assets.precompile += %w( jquery.tinysort.js )
Rails.application.config.assets.precompile += %w( jquery.timer.js )
Rails.application.config.assets.precompile += %w( leaderboard.js )
Rails.application.config.assets.precompile += %w( login.js )