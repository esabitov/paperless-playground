class AddSeveralColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :rep_id, :string
    add_column :users, :gf_region_name, :string
    add_column :users, :title, :string
    add_column :users, :position_desc, :string
    add_column :users, :role_at_meeting, :string
    add_column :users, :territory_num, :string
    add_column :users, :district_name, :string
    add_column :users, :salesforce, :string
  end
end
