**Dockerhub URL**

https://hub.docker.com/r/gramercyhub/paperless-playground/

Use docker-compose.yml to docker-compose up.

After docker-compose up, the database needs to be setup using:

```
#!command

docker exec paperlessplayground_web_1 rake db:setup
```
 
You may need to replace paperlessplayground_web_1 with the correct name of the container. This will setup the database.