class AdminController < ApplicationController
layout 'admin'

  def index
    @users = User.all
  end

  def admin_new
    @all_districts = User.total_districts
    @user = User.new
  end

  def admin_create
    if @user = User.find_by(username: params['user']['username'])
      flash[:notice] = "User #{params['user']['username']} already exists"
      redirect_to admin_path
    else
      @user = User.new(user_params)
      if @user.save
        redirect_to admin_path
      else
        @all_districts = User.total_districts
        render 'admin_new'
      end
    end
  end

  def pdfs 
    @items = Dir.entries("public/pdf")
    render 'pdfs', :items => @items
  end
  
  private

    def user_params
      @task = params[:user].permit(:username, :district_name)
    end
end
