class UsersController < ApplicationController
layout 'login'

  def index
    redirect_to root_path
  end

  def new
    @user = User.new
    if cookies[:username]
      if @user = User.find_by(username: cookies[:username])
        redirect_to @user
      else
        # If the cookie exists but not the associated user,
        # remove the cookies to avoid a feedback loop
        remove_cookies
        redirect_to root_path
      end
    end
  end

  def create
    username = params["user"]["username"]
    if @user = User.where('lower(username) = lower(?)', username)[0]
      set_cookies(@user)
      redirect_to @user
    else
      flash[:notice] = "User doesn't exist"
      redirect_to root_path
    end
  end



  # def create
  #   if @user = User.find_by(username: params['user']['username'])
  #     set_cookies(@user)
  #     redirect_to @user
  #   else
  #     @user = User.new(user_params)
  #     @user.save
  #     render 'new'
  #   end
  # end


  # def create
  #   if @user = User.find_by(username: params['user']['username'])
  #     set_cookies(@user)
  #     redirect_to @user
  #   else
  #     @user = User.new(user_params)
  #     if @user.save
  #       set_cookies(@user)
  #       redirect_to @user
  #     else
  #       render 'new'
  #     end
  #   end
  # end

  def show
    @user = User.find(params[:id])
    redirect_to '/playground/index'
  end

  def log_out
    remove_cookies
    redirect_to root_path
  end

  def destroy
    @user = User.find(params[:id])
    # If you're deleting the currently logged-in user, delete the cookies as well
    remove_cookies if cookies[:username] == @user.username
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_path, notice: 'User was successfully removed.' }
      format.json { head :no_content }
    end
  end


  private

    def user_params
      @task = params[:user].permit(:username)
    end

    def remove_cookies
      cookies.delete(:username)
      cookies.delete(:user_id)
    end

    def set_cookies(user)
      cookies[:username] = user.username
      cookies[:user_id] = user.id
    end

end

