class PagesController < ApplicationController


  def index
    @user = User.new
    if cookies[:username]
      @user = User.find_by(username: cookies[:username])
      redirect_to @user
    end
  end

end
