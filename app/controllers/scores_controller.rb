class ScoresController < ApplicationController
layout 'admin'

  def new_from_endpoint
  end

  def admin_new
    @score = Score.new
  end

  def admin_create
    if !Score.exists?(game_id: params[:game_id], user_id: params[:user_id])
      @score = Score.new(:game_id => params[:game_id], points: params[:points], :user_id => params[:user_id] )
      @score.save
      render json: @score
    else
      render json: {success: false}
    end
    #redirect_to admin_path
  end

  def new
    @user = User.find(params[:user_id])
    @username = @user.username
    @score = Score.new
    @games = Game.all
  end

  def create
    game = Game.find_by(name: score_params[:game_id])
    @score = Score.new(score_params)
    @score.game_id = game.id
    @score.save
    redirect_to admin_path
  end

  def edit
    @score = Score.find(params[:id])
  end

  def destroy
    Score.find(params[:id]).destroy
    respond_to do |format|
      format.html { redirect_to admin_path, notice: 'Score was successfully removed.' }
      format.json { head :no_content }
    end
  end

  def update
    @score = Score.find(params[:id])
    if @score.update_attributes(score_params)
      flash[:success] = "Score updated"
      redirect_to admin_path
    else
      render 'edit'
    end
  end

  private

    def score_params
      params.require(:score).permit(:points, :user_id, :game_id)
    end



end
