class LeaderboardsController < ApplicationController
layout 'leaderboard'

  def index
    @ranking_list = Score.total_ranking.take(10)
  end

  def index_json
    @overall_scores = Score.total_ranking.take(10)
    render json: @overall_scores 
  end

  def create
    # game_id = Integer(params["game_id"])
    # leaderboard = Leaderboard.find(game_id)
    # @ranking_list = leaderboard.ranking(game_id)
    # render json: @ranking_list  
  end

  def show_json
    game_id = params["game_id"]
    leaderboard = Leaderboard.find(game_id)
    @ranking_list = leaderboard.ranking(game_id)
    render json: @ranking_list
  end

  def show
    @game_id = params["id"]
    @leaderboard = Leaderboard.find(params["id"])
    @ranking_list = @leaderboard.ranking(@game_id)
  end

private

  def leaderboard_params
    params.require(:leaderboard).permit(:gameID)
  end

end

