class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  skip_before_action :verify_authenticity_token
  before_action :verify_custom_authenticity_token

  def verify_custom_authenticity_token
    # checks whether the request comes from a trusted source
  end

end
