class Leaderboard < ApplicationRecord

  def get_user_name(user_id)
    User.find(user_id).username
  end

  def return_game_specific_info(game_id)
    sorted_scores = Score.where(game_id: game_id).order(points: :desc).limit(10)
    sorted_scores.map do |score|
      {username: get_user_name(score.user_id), points: score.points}
    end
  end

  def ranking(game_id)
    ranking = 1
    new_array = []
    json_array = return_game_specific_info(game_id)
    json_array.each_with_index do |object, index|
      if index == 0
       new_array << { ranking: ranking, object: object}
      else 
        if json_array[index-1][:points] == object[:points]
          new_array << { ranking: ranking, object: object}
        else 
          ranking += 1
          new_array << { ranking: ranking, object: object}
        end
      end
    end
    new_array
  end

  def overall_ranking

  end



end

# puts JSON.pretty_generate(obj)
