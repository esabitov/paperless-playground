class Score < ApplicationRecord
  belongs_to :user
  belongs_to :game

  def get_game_name
    game = Game.find_by(id: self.game_id)
    return game.name
  end

  # def self.total_points_per_user
  #   Score.all.group_by(&:user_id).map do |obj|
  #     points_array = []
  #     # {id_for_user: obj[0], points: points_array}
  #     counter = 1
  #     total_users = User.count
  #     obj[1].map do |score_obj|
  #       score_obj.points
  #     end
  #   end
  # end

  def self.total_user_points
    Score.all.group_by(&:user_id).map do |obj|
      {username: User.find(obj[0]).username, points: obj[1].map { |points_obj| points_obj.points }.inject {|sum, n| sum + n}} 
    end
  end

  def self.sort_by_points
    Score.total_user_points.sort_by {|k,v| k[:points]}.reverse
  end

# BELOW METHOD WAS USED WHEN AGGREGRATING USERS BUT CHANCE TO TAKE INTO ACCOUNT DISTRICTS

# def self.total_ranking
#   ranking = 1
#   new_array = []
#   json_array = Score.sort_by_points
#   json_array.each_with_index do |obj, index|
#     if index == 0
#       new_array << { ranking: ranking, object: obj}
#     else
#       if json_array[index-1][:points] == obj[:points]
#         new_array << { ranking: ranking, object: obj}
#       else 
#         ranking += 1
#         new_array << { ranking: ranking, object: obj}
#       end
#     end
#   end
#   new_array
# end

def self.total_ranking
  ranking = 1
  new_array = []
  json_array = User.sql_final_score
  json_array.each_with_index do |obj, index|
    if index == 0
      new_array << { ranking: ranking, object: obj}
    else
      if json_array[index-1]["score"] == obj["score"]
        new_array << { ranking: ranking, object: obj}
      else 
        ranking += 1
        new_array << { ranking: ranking, object: obj}
      end
    end
  end
  new_array
end
# .sort_by {|k,v| v[:points]}.reverse

  # def self.associate_rankings
  #   ranking = 1
  #   new_array = []
  #   # json_array = Score.total_points_per_user
  #   json_array = Score.total_points_per_user.map {|obj| obj.sum}
  #   binding.pry
  #   json_array.each_with_index do |object, index |
  #     if index == 0 
  #     new_array << { ranking: index + 1, total_points: object.inject {|sum, n| sum + n}}
  #     end
  #   end
  #   new_array.sort_by {|k,v| v}.reverse
  # end

  # def self.total_points_per_user
  #   new_array = []
  #   total_users = User.count
  #   counter = 1
  #   scores = Score.all.order(user_id: :asc) 
  #   scores.map do |score_obj|
  #     if score_obj.user_id == counter 
  #       binding.pry
  #     else 
  #       counter += 1
  #     end
  #     if counter > total_users
  #       break
  #     end
  #   end
  #   puts "hello world #{counter} #{total_users}"
  # end

end
