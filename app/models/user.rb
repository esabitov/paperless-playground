require 'csv'    

class User < ApplicationRecord
  has_many :scores, dependent: :destroy

  validates :username, presence: true

  validates_format_of :username, :with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/

  def create_scores
    games = Game.all
    games.each do |game|
      s = Score.new
      s.game_id = game.id
      s.user_id = self.id
      s.points = 0
      s.save
    end

  end

  def self.total_districts
    total_districts = User.all.group_by(&:district_name).keys
    total_districts.reject {|district| district == "" || district == nil}.sort
  end
  # => ["", "West District", "East District", "Central District", "Boston, MA", ...]

  def self.overall_count_of_districts
    User.total_districts.map do |district_name|
      {district: district_name, num_of_people: User.all.where("district_name = ?", district_name).where("district_name != 'Home Office'").where("district_name != 'Medical'").where("district_name != ''").where("district_name != 'Dome'").count }
    end.sort_by {|obj| obj[:num_of_people] }.reverse
  end
  # => [{:district=>"", :num_of_people=>121}, {:district=>"New York, NY", :num_of_people=>27}, ...]

  def self.find_users_per_district(district_name)
    User.all.group_by(&:district_name)[district_name]
  end
  # => #<User id: 111, created_at: "2016-09-19 20:52:49", updated_at: "2016-09-19 20:52:49" ...]

  def self.get_user_ids_per_district(district_name)
    User.find_users_per_district(district_name).map {|obj| obj[:id]}
  end

  def self.get_scores_per_district(district_name)
    scores_hash = {district_name: district_name, score: score_array = []}
    User.get_user_ids_per_district(district_name).each do |user_id|
      # {district_name: district_name, score: Score.find_by(user_id: user_id).points}
      # {district_name: district_name, score: score_array << Score.find_by(user_id: user_id).points }
      if !Score.find_by(user_id: user_id).nil?
        score_array << Score.find_by(user_id: user_id).points
      end
    end
    scores_hash[:score] = scores_hash[:score].sum
    scores_hash
  end

  def self.get_scores_for_all_districts
    district_scores_array = []
    all_districts = User.total_districts
    all_districts.each do |district|
      district_scores_array << User.get_scores_per_district(district)
    end
    district_scores_array.sort_by {|obj| obj[:score]}.reverse
  end

  def self.ruby_final_score
    return_array = []
    sql = "SELECT DISTINCT district_name, 
    COALESCE((
     SELECT (
       SUM(scores.points)
    ) 
    FROM users 
    JOIN scores on scores.user_id = users.id 
    WHERE users.district_name = u.district_name
    ), 0) 
    AS score FROM users AS u 
    WHERE district_name != ''
    AND district_name != 'Home Office'
    AND district_name != 'Medical'
    AND district_name != 'Dome'
    ORDER BY score DESC"
    records_array = ActiveRecord::Base.connection.select_all(sql)
    highest_num_in_district = User.overall_count_of_districts[0][:num_of_people]
    records_array.map do |record|
       weighted = highest_num_in_district / User.get_user_ids_per_district(record["district_name"]).count.to_f
       record["score"] = (record["score"] * weighted).round
       record
    end.sort_by {|obj| obj["score"]}.reverse
  end

  def self.sql_final_score
  sql = "SELECT DISTINCT district_name,
  COALESCE((
    SELECT ( 
      CEILING (
        SUM(scores.points) * (
           SELECT MAX(
                   (SELECT COUNT(id)
                    FROM users
                    WHERE district_name = u.district_name)
          ) num_of_users
              FROM users AS u
              WHERE district_name != ''
              AND district_name != 'Home Office'
              AND district_name != 'Medical'
              AND district_name != 'Dome'
        ) / (
          SELECT COUNT(id)
              FROM users
              WHERE users.district_name = u.district_name
        )
      )
    )
     FROM users
     JOIN scores ON scores.user_id = users.id
     WHERE users.district_name = u.district_name 
  ),0)
  AS score 
  FROM users AS u
  WHERE district_name != ''
  AND district_name != 'Home Office'
  AND district_name != 'Medical'
  AND district_name != 'Dome'  
  ORDER BY score DESC"
  records_array = ActiveRecord::Base.connection.select_all(sql)
  end


# User.social_media_sheet('csv-example1.csv', 1)
  def self.social_media_sheet(csv_file_name, game_id)
    csv_text = File.read('CSV/' + csv_file_name)
    # csv = CSV.parse(csv_text, :headers => true)
    csv = CSV.parse(csv_text)
    total_points = 0
    csv.each_with_index do |row, index|
      cur = csv[index][0]
      user = User.find_by(username: cur)
      if user
        if csv.length - 1  == index
          #last row so just put total in the db
          total_points += csv[index][-1].to_i
          Score.create(user_id: user.id , game_id: game_id, points: total_points)
        else  
          next_m = csv[index+1][0]
          total_points += csv[index][-1].to_i
          if cur != next_m
          Score.create(user_id: user.id , game_id: game_id, points: total_points)
          total_points = 0
          end 
        end
      end
    end
  end

  def self.find_lcm
    # Make sure 0 is not included in the array
    User.overall_count_of_districts.map do |obj|
      obj[:num_of_people]
    end.uniq.reduce(:lcm)
  end
  #  => 1367566200 

end

