/*
*
*   Author: Elliot Sabitov
*   Company: Gramercy Tech
*   Description: Lightbox scripts, ready scope.
*
*
*/
//opens lightbox
function lightbox_show(){
    stop_audio();
    clearTimeout(main_loop_interval);
    //clearInterval(highlight_interval);
    $('.lightbox').fadeIn(300);
    setTimeout(function(){
        $('.lightbox').addClass('open');
    },300);
    /*if(cur_status.room_num == 3){
        stop_render();
    }*/
}
//closes lightbox
close_lightbox = function(){
    stop_audio();
    //got a bug where the drag and drop activity locked mouse cursor on move. So im gonna check for it and clear it
    if($('body').css('cursor') == 'move'){
        //this can probably be somehow optimized and moved to only happen during that one activity.
        $('body').css('cursor','auto');
    }
    /*if(cur_status.room_num == 3){
        start_render();
    }*/
    main_loop_interval = setTimeout( loop, 15);
    //highlight_interval = setInterval(function(){$('.highlight').toggleClass('hover');},750);
    var slide_id = $('.lightbox-content .current').attr('id');
    $('.lightbox').fadeOut(300);
    $('.lightbox').removeClass('open');
    $('.lightbox-content').removeClass('dark height-auto');
    if( (($('.lightbox-content .current').index()-1 == $('.lightbox-content .slide').length) &&
        ($('.lightbox-content .current').hasClass('completed')))
        || ($('.lightbox-content').attr('name').indexOf('_objectives') !== -1)){
        cur_status.finished($('.lightbox-content').attr('name'));
    }
    //if we are in room 2
    if(cur_status.room_num == 2){
        attempt = 0;
        $('.lightbox-content').off(device_click_event,'.checkbox');
        //$('.lightbox-content').off(device_click_event,'.checkbox', handle_checkbox_click_event).on(device_click_event, '.checkbox', handle_checkbox_click_event);
    }   
    //$('.nav-circle-absolute').remove(); 
    $('.lightbox-content').html('');
    //window[$('.lightbox-content').attr('name')].complete = 1;
    if(slide_id==16){
        //clear all timeouts. I did this because stuff was still going on if you left slide 16 and came back.
        //this may be good to be made global to clear all timeouts and intervals that pertain to the lightbox content
        var id = window.setTimeout(function() {}, 0);
        while (id--) {
            window.clearTimeout(id); // will do nothing if no timeout with id is present
        }
    }
    if(getCookie('cur') !== null){
        deleteCookie('cur');
    }
    slide_id = null;
}
//adds the circles that indicate how many slides are in the lightbox sequence. The circles who up on the bottom
//middle section of the lightbox (if there are more than one slide)
add_circles = function(){
    $('.nav-circle-absolute').remove();
    var first = $('.lightbox-content .slide:eq(0)').index();
    var num = $('.current').index();
    num = num - first;
    var len = $('.lightbox-content .slide').length;
    var s1 = '<div class="nav-circle active"></div>';
    var s2 = '<div class="nav-circle"></div>';
    var str = '';
    var s;
    if(num == 0){
        str = s1;
    }
    else{
        str = s2;
    }
    for(var i = 1; i<len; i++){
        if(num == i){
            str = str.concat(s1);
        }
        else{
            str = str.concat(s2);
        }
        
    }
    var start = '<div class="nav-circle-absolute"><div class="nav-circle-wrap">';
    str = start.concat(str);
    str = str.concat('</div></div>');
    $('.slides').append(str);
};
//should run every time next and previous is pressed, updates circle breadcrumb
//this just updates which circle is current
update_circles = function(){
    var first = $('.lightbox-content .slide:eq(0)').index();
    var num = $('.lightbox-content .current').index();
    num = num - first;
    $('.nav-circle.active').removeClass('active');
    $('.nav-circle').eq(num).addClass('active');
};
//function is fired every time next and previous is pressed.
//add_prev_next function is used to turn on/off the prev/next arrows based on whether the slides_unlocked is true
//or false. Basically, if the user has to perform some sort of activity to complete the slide, then slides_unlocked
//will be false until the user does what they have to. (Ex. Listening to the entire mp3 and waiting for it to finish
//will have a callback that will set slides_unlocked to true after the audio finishes.)
add_prev_next = function(){
    var el = $('.current');
    //rules for next button to appear.
    if(el.hasClass('completed')){
        if(el.next('.slide').length > 0){
            $('.next').addClass('available');
        }
        else{
            $('.next').removeClass('available');
        }
        if(el.prev('.slide').length > 0){
            $('.prev').addClass('available');
        }
        else{
            $('.prev').removeClass('available');
        }
        slides_unlocked = true;
    }else{
        if(slides_unlocked){
            if(el.next('.slide').length > 0){
                $('.next').addClass('available');
            }
            else{
                $('.next').removeClass('available');
            }
            if($('.current').next('.slide').length == 0){
                $('.next').removeClass('available');
                $('.lightbox').addClass('open');
            }
        }
        else{
            $('.prev, .next').removeClass('available');
        }
        //previous is always lit unles current.prev doesnt exist.
        if(el.prev('.slide').length > 0){
            $('.prev').addClass('available');
        }
        else{
            $('.prev').removeClass('available');
        }
    }
    el = null;
};
//handle_completed_asset_helper is used within every room_x_asset_y asset function
//basically used to set the appropriate .current, .before and .after classses to the slides
//based on where the user is in the activity sequence. Room_x.js functions will use handle_completed_asset_helper
//to specify which room/asset we are working with. The slides parameter is to identify which .slides_x div will be 
//rendered from the room_x.html. The v is the string that would be passed that would indicate the exact location
//of the user in the mod. Ex. room_x_asset_y_slide_4.
handle_completed_asset_helper = function(room,asset,slides,v){
    //console.log('room_'+room+'_asset_'+asset+'_slides_'+slides+'_v_'+v);
    v = typeof v !== 'undefined' ?  v : false;
    var completed = getCookie('completed');
    var slide_num = 0;
    var completed_num = 0;
    var all_completed = false;
    slides_unlocked = false;
    if(completed !== null){
        completed = completed.split('^');
        if(completed.length){
            for(var i = 0; i<completed.length;i++){
                if(completed[i].indexOf('room_'+room+'_asset_'+asset+'_slide_')!==-1){
                    slide_num = parseInt(completed[i].substr(completed[i].indexOf('slide_')+6));
                    completed_num = slide_num;
                    //20160603 they want it to start from beginning no matter what so setting slide_num to 0
                    slide_num = 0;
                }
                else if(completed[i] == 'room_'+room+'_asset_'+asset){
                    all_completed = true;
                }
            }
        }
    }
    $('.lightbox-content').attr('name','room_'+room+'_asset_'+asset+''); 
    $('.lightbox-content').html('');           
    $('.lightbox-content').html($('.slides_'+slides+'').clone());
    //if current slide is last slide AND current slide is COMPLETED then make it first.
    if($('.lightbox-content .slide').length - 1 == slide_num && $('.lightbox-content .slide.slide_'+slide_num).hasClass('completed')){
        slide_num = 0;
    }
    if(v!==false){
        if(all_completed){
            //20160603 $('.lightbox-content .slides_'+slides+' .slide').addClass('before completed');
            //removed before because otherwise it slides in backwards
            $('.lightbox-content .slides_'+slides+' .slide').addClass('completed');
            if($('.lightbox-content').attr('name') === 'room_3_asset_1'){
                //extra shit for apc because the next prev buttons need to be hidden only when completed by not when all completed
                //wow fuck my life for them having this dumbass idea.
                $('.lightbox-content .slides_'+slides+' .slide').addClass('apc-completed');
            }
            /*20160603  guess were not showing answers at all any more.
            show_answers();
            */
        }
        else{
            //20160603 if not all completed then it means that they either closed out of an activity
            //clicked in the menu or refreshed
            $('.lightbox-content .slides_'+slides+' .slide:nth-child(-n+'+(completed_num+2)+')').addClass('before completed');
        }
        slide_num = parseInt(v.substr(v.indexOf('slide_')+6));
    }
    else{
        if(all_completed){
            //if completed start from beginning but give them left right buttons to let them switch
            //20160603 $('.lightbox-content .slides_'+slides+' .slide').addClass('before completed');
            //removed before  because otherwise it slides in backwards
            $('.lightbox-content .slides_'+slides+' .slide').addClass('completed');
            if($('.lightbox-content').attr('name') === 'room_3_asset_1'){
                //extra shit for apc because the next prev buttons need to be hidden only when completed by not when all completed
                //wow fuck my life for them having this dumbass idea.
                $('.lightbox-content .slides_'+slides+' .slide').addClass('apc-completed');
            }
            /*20160603  guess were not showing answers at all any more.
            show_answers();
            */
        }
        else{
            //20160603 if they are not complete then just start from beginning
            //$('.lightbox-content .slides_'+slides+' .slide:nth-child(-n+'+(completed_num+2)+')').addClass('before completed');
        }
    }
    $('.lightbox-content .slides_'+slides+' .slide:eq('+slide_num+')').removeClass('before after').addClass('current');
    setCookie('cur','room_'+room+'_asset_'+asset+'_slide_'+slide_num);
    add_circles();
    add_prev_next();
    if(slide_num > 0){
        $('.lightbox-content .prev').addClass('available');
    }
    lightbox_show();
    clearTimeout(main_loop_interval);
}
//lightbox_init delegates the .close button click for the lightbox. 
//This has a callback because in some rare cases I had to perform a custom action
//after the lightbox was closed.
lightbox_init = function(callback){
    var callback = typeof callback !== 'undefined' ?  callback : false;
    $('.lightbox').off(device_click_event,'.close');
    $('.lightbox').on(device_click_event,'.close', function(e){
        if(good_click(e) && $('.lightbox').hasClass('open')){
            if(callback !== false){
                close_lightbox();
                callback();
            }else{
                close_lightbox();
            }
        }
    });
}
 //Other global scope functions
handle_checkbox_click_event = function(event){
    handle_checkbox_click($(this),event);
}

//shows learning objectives for the room
display_learning_objectives = function(rm,callback){
    var callback = typeof callback !== 'undefined' ?  callback : false;
    $('.lightbox-content').attr('name','room_'+rm.id+'_objectives'); 
    $('.lightbox-content').append(rm.get_learning_objectives());
    $('.lightbox').addClass('open');
    $('.slide_0 .close').addClass('available');
    lightbox_show();
    if(callback !== false){
        $('.lightbox').off(device_click_event,'.close');
        $('.lightbox').on(device_click_event,'.close', function(e){
            if(good_click(e) && $('.lightbox').hasClass('open')){
                close_lightbox();
                callback();
                lightbox_init();
            }
        });
    }
}

//handles all video slides. Delegates the video .ended callback and makes slide complete/unlocked after video is ended.
handle_video_slide = function(vid_id,callback){
    var callback = typeof callback !== 'undefined' ?  callback : false;
    var vid = document.getElementById('slide_'+vid_id+'_video');
    //play audio and switch images
    vid.play();
    vid.addEventListener('ended', function(){
        $(vid).closest('.slide').addClass('completed');
        slides_unlocked = true;
        add_prev_next();
        this.removeEventListener('ended', arguments.callee);
        if(callback !== false){
            callback();
        }
    });
}

//shows the correct answers. Was previously used for completed activites. May no longer be used because they wanted to 
//restart the activity from beginning after every load or lightbox open
show_answers = function(){
    $('.lightbox-content .answer').addClass('correct');
}