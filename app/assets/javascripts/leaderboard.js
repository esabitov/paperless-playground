$(document).on('ready', function(){

  var gameID = $(".leaderboard-number").text()

    setInterval(function(){
      $.ajax({ url: '/index_json',
        type: 'POST',
        beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
        data: {game_id: gameID},
        success: function(response) {
          $.each(response,function(i){
            var row = response[i];
            var inc_rank = row.ranking;
            var inc_email = row.object.district_name;
            var inc_points = row.object.score;
            if($('.lb-table .header-table tr:eq('+i+') td:eq(0) > div').length){ //table row exists so update
              var cur_rank = $('.lb-table .header-table tr:eq('+i+') td:eq(0) > div');
              var cur_email = $('.lb-table .header-table tr:eq('+i+') td:eq(1) > div');
              var cur_points = $('.lb-table .header-table tr:eq('+i+') td:eq(2) > div');
              if(cur_rank.text().slice(0,-1) != inc_rank){
                //update col 1
                cur_rank.addClass('spin');
                setTimeout(function(){
                  cur_rank.text(inc_rank + '.');
                },500);
                setTimeout(function(){
                  cur_rank.removeClass('spin');
                },1000);
              }
              if(cur_email.text() != inc_email){
                //update col 2
                cur_email.addClass('spin');
                setTimeout(function(){
                  cur_email.text(inc_email);
                },500);
                setTimeout(function(){
                  cur_email.removeClass('spin');
                },1000);
              }
              if(cur_points.text() != inc_points){
                //update col 3
                cur_points.addClass('spin');
                setTimeout(function(){
                  cur_points.text(inc_points);
                },500);
                setTimeout(function(){
                  cur_points.removeClass('spin');
                },1000);
              }
            }
            else {
              //create new row in table
              $('.lb-table .header-table').append(
                '<tr class="tr-background remove">'+
                  '<td><div class="delete-ranking'+i+'">'+inc_rank+'.</div></td>'+
                  '<td><div class="delete-name'+i+'">'+inc_email+'</div></td>'+
                  '<td><div class="delete-score'+i+'">'+inc_points+'</div></td>'+
                '</tr>'
              );
              setTimeout(function(){
                $('.lb-table .header-table .remove').removeClass('remove');
              },250);
            }
          });
          //now we need to remove any rows that need to be removed
          //so lets get count of response and compare it to the count of table rows
          //if table rows > count of response then remove the additional table rows
          setTimeout(function(){  //lets wait for previous updates to all happen first
            var num_of_rows = parseInt($('.lb-table .header-table tr').length);
            var num_of_rows_to_remove_from_end = num_of_rows - response.length;
            if(num_of_rows_to_remove_from_end > 0){
              //get index of the row to be removed
              for(var i = 0; i < num_of_rows_to_remove_from_end; i++){
                $('.lb-table .header-table tr:eq('+(num_of_rows - i - 1)+')').addClass('remove');
              }
              setTimeout(function(){
                $('.lb-table .remove').remove();
              },500);
            }
          },1000);
          cur_rank = null;
          cur_email = null;
          cur_points = null;
          inc_rank = null;
          inc_email = null;
          inc_points = null;
        }
      });
    }, 3000);
  });
