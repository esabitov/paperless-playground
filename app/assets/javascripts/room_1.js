//===============room_1==============================
//first lets clear anything we dont need to help with memory performance.
//room_0_vars

handle_next_click = null;
handle_prev_click = null;
room_1 = null;
room_1_asset_1 = null;
room_1_asset_2 = null;
room_1_asset_3 = null;
room_1_asset_4 = null;

function tally_score(){
    //this function will add the cur slide score to the over all asset/game score
    //get the current slide score
    var cookie_name = 'score_game_'+window[$('.lightbox-content').attr('name')].game_id;
    var slide_score = $('.lightbox-content .current').data('score') ? $('.lightbox-content .current').data('score') : 0;
    var cur_game_score = getCookie(cookie_name);
    cur_game_score = cur_game_score ? cur_game_score : 0;
    setCookie(cookie_name,(parseInt(cur_game_score)+parseInt(slide_score)));
    cur_game_score = null;
    slide_score = null;
    cookie_name = null;
}

function delegate_s11f_click(){
    $('.lightbox-content .current .submit, .lightbox-content .current .s11f-a').off(device_click_event);
    //delegate only if slide isnt completed.
    if(!$('.lightbox-content .current').hasClass('completed')){
        $('.lightbox-content .current .submit').on(device_click_event, function(e){
            if(good_click(e)){
                if($('.lightbox-content .current .sel').length > 0){
                    handle_s11f_selection();
                }
            }
        });
        $('.lightbox-content .current .s11f-a').on(device_click_event,function(){
            if(!$(this).hasClass('sel')){
                $('.lightbox-content .current .sel').removeClass('sel');
                $(this).addClass('sel');
            }
        });
    }
}
function handle_s11f_selection(){
    var btn = $('.lightbox-content .current .sel').eq(0);
    $('.lightbox-content .current .submit, .lightbox-content .current .s11f-a').off(device_click_event);
    $('.lightbox-content .current .slide-content-wrap').removeClass('show').addClass('hide');
    var next_str = 'NEXT QUESTION';
    //if last slide made next_str continue
    if($('.lightbox-content .current').index() - 1 == $('.lightbox-content').find('.slide').length){
        next_str = 'CONTINUE';
    }
    if(btn.hasClass('answer')){ //correct
        tally_score();
        var additionalFeedback = '';
        if (btn.hasClass('explanation')) {
            additionalFeedback = btn.data('name');
        }
        else {
            additionalFeedback = 'That is correct!';
        }
        if($('.lightbox-content .current').hasClass('slide_19')){
            $('.current .feedback').html(
                '<div class="display-table">'+
                    '<div class="display-cell">'+
                        '<div class="correct-msg">' + additionalFeedback + '</div>'+
                        '<img src="/assets/answer.png" class="full-answer"/>'+
                        '<div class="continue-btn btn">'+
                            '<div class="continue-btn-child btn-child">'+
                                next_str+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        else{
            $('.current .feedback').html(
                '<div class="display-table">'+
                    '<div class="display-cell">'+
                        '<div class="correct-msg">' + additionalFeedback + '</div>'+
                        '<div class="continue-btn btn">'+
                            '<div class="continue-btn-child btn-child">'+
                                next_str+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        $('.lightbox-content .current .feedback').addClass('show').removeClass('hide');
        handle_s11f_continue();
    }
    else{ //choise incorrect
        var answer = $('.lightbox-content .current .s11f-a.answer').parent().text();
        if($('.lightbox-content .current').hasClass('slide_19')){
            $('.current .feedback').html(
                '<div class="display-table">'+
                    '<div class="display-cell">'+
                        '<div class="incorrect-msg">Incorrect!</div>'+
                        '<div class="expl">The correct answer is shown below:</div>'+
                        '<img src="/assets/answer.png" class="full-answer"/>'+
                        '<div class="continue-btn btn">'+
                            '<div class="continue-btn-child btn-child">'+
                                next_str+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        else{
            $('.current .feedback').html(
                '<div class="display-table">'+
                    '<div class="display-cell">'+
                        '<div class="incorrect-msg">Incorrect!</div>'+
                        '<div class="expl">The correct answer is <strong>' + answer + '</strong></div>'+
                        '<div class="continue-btn btn">'+
                            '<div class="continue-btn-child btn-child">'+
                                next_str+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        $('.lightbox-content .current .feedback').addClass('show').removeClass('hide');
        handle_s11f_continue();
        answer = null;
    }
}

function handle_s11f_off_click(){ //now not used because it is single attempt now.
    $('.lightbox-content .current .s11f-a').removeClass('correct incorrect');
    $('.current .feedback').html('');
    delegate_s11f_click();
}

function handle_s11f_continue(){
    $('.lightbox-content .current .submit, .lightbox-content .current .s11f-a').off(device_click_event);
    setTimeout(function(){
        $('.lightbox-content .current.slide').addClass('completed');
        slides_unlocked = true;
        add_prev_next();
        attempt = 0;
    },100);
}


var cur = getCookie('cur');
var completed = getCookie('completed');
if(completed !== null){
    completed = completed.split('^');
}
$('.loaded_content').load('/html/room_1.html', function(){
    //loop through completed and call asset function if necessary
    var latest = false;
    if(cur != null && cur.indexOf('_slide_') > -1){
        latest = cur.substr(0,cur.indexOf('_slide_'));
        var n =  latest + '_function';
        window[n](cur);
    }
});
//Room(id,x,y,z,rx,ry,rz,ax_min,ay_min,az_min,ax_max,ay_max,az_max,locked)
var room_1 = new Room(1,19,-6,0,265,0,-180,700,500,200,-130,1000,200);
//sequence.add(room_1);
// floor
room_1.add( new Plane("url('/assets/crackedConcrete-min.png');background-repeat:repeat;", 2600, 2600, -1300, 1300, 490, 180, 0, 0));
// ceil
room_1.add( new Plane("url('/assets/crackedConcrete-min.png');background-repeat:repeat;", 2000, 2000, -1000, -1000, -310, 0, 0, 0));
// walls
room_1.add( new Plane("url('/assets/wall_fireplace-min.png');background-size:cover;", 1270, 1320, 635, -635, -490, 270, 0,180));   //left
room_1.add( new Plane("url('/assets/wall_door-min.png');background-size:cover;", 1270, 1320, -635, -635, -490, 270, 90, 180));   //front
room_1.add( new Plane("url('/assets/wall_globe-min.png');background-size:cover;", 1270, 1320, -635, 635, -490, 90, 0, 0)); //right
room_1.add( new Plane("url('/assets/wall_safe-min.png');background-size:cover;", 1270, 1320, 635, 635, -490, 90, 270, 0));        //back
//door
//room_1.add( new Plane("transparent;", 260, 530, 504, -104, -276, 90, 270, 0,'room_0'));

// var room_1_asset_1_function = function(v){
//     handle_completed_asset_helper(1,1,1,v);
//     if($('.lightbox-content .current').is('.slide_1,.slide_3')){    
//         delegate_s11f_click();
//     }
//     else if($('.lightbox-content .current').hasClass('slide_2')){
//         $(".s6b-a").draggable({disabled: true});
//         $('.s6b-d').droppable({disabled: true});
//         $(".s6b-a").draggable( {
//             opacity: .4,
//             create: function(){
//                 $(this).data('position',$(this).position());
//             },
//             //cursorAt:{left:15},
//             cursor:'move',
//             revert: 'invalid',
//             start:function(){
//                 $(this).stop(true,true);
//             },
//             stop: function(){
//             },
//             disabled:false
//         });
//         $('.s6b-d').droppable({
//             over: function(event, ui) {
//                 $(ui.helper).unbind("mouseup");
//             },
//             drop:function(event, ui){
//                 snapToMiddle(ui.draggable,$(this));
//             },
//             out:function(event, ui){
//                 $(ui.helper).mouseup(function() {
//                     snapToStart(ui.draggable,$(this)); 
//                 });
//             },
//             disabled:false
//         });
//         $('.lightbox-content .current .submit').off(device_click_event).on(device_click_event,function(){
//             check_drag_complete();
//         });
//     }
// }

// var room_1_asset_1_vars = {
//     name:'room_1_asset_1',
//     css: 'border-radius: 20% 20% 40% 40%;',
//     w:107,
//     h:169,
//     x:51,
//     y:-633,
//     z:-249,
//     rx:270,
//     ry:0,
//     rz:180,
//     game_id:1,
//     timestamp:1474741200,
//     access_code:'44',
//     //timestamp:1474575240,
//     fun: function(){
//         room_1_asset_1_function();
//     }
// };
// var room_1_asset_1 = new Asset(room_1_asset_1_vars, cur_status);
// room_1.add(room_1_asset_1);

var room_1_asset_1_function = function(v){
    handle_completed_asset_helper(1,1,1,v);
    if($('.lightbox-content .current').is('.slide_1,.slide_3')){    
        delegate_s11f_click();
    }
    else if($('.lightbox-content .current').hasClass('slide_2')){
        $(".s6b-a").draggable({disabled: true});
        $('.s6b-d').droppable({disabled: true});
        $(".s6b-a").draggable( {
            opacity: .4,
            create: function(){
                $(this).data('position',$(this).position());
            },
            //cursorAt:{left:15},
            cursor:'move',
            revert: 'invalid',
            start:function(){
                $(this).stop(true,true);
            },
            stop: function(){
            },
            disabled:false
        });
        $('.s6b-d').droppable({
            over: function(event, ui) {
                $(ui.helper).unbind("mouseup");
            },
            drop:function(event, ui){
                snapToMiddle(ui.draggable,$(this));
            },
            out:function(event, ui){
                $(ui.helper).mouseup(function() {
                    snapToStart(ui.draggable,$(this)); 
                });
            },
            disabled:false
        });
        $('.lightbox-content .current .submit').off(device_click_event).on(device_click_event,function(){
            check_drag_complete();
        });
    }
    else if($('.lightbox-content .current').is('.slide_4,.slide_5,.slide_6')){    
        delegate_s11f_click();
    }
}

var room_1_asset_1_vars = {
    name:'room_1_asset_1',
    w:582,
    h:322,
    x:286,
    y:-624,
    z:107,
    rx:270,
    ry:0,
    rz:180,
    game_id:1,
    access_code:'90',
    fun: function(){
        room_1_asset_1_function();
    }
};
var room_1_asset_1 = new Asset(room_1_asset_1_vars, cur_status);
room_1.add(room_1_asset_1);

var room_1_asset_2_function = function(v){
    handle_completed_asset_helper(1,2,2,v);
    if($('.lightbox-content .current').is('.slide_7,.slide_8,.slide_9,.slide_10,.slide_19')){    
        delegate_s11f_click();
    }
}
var room_1_asset_2_vars = {
    name:'room_1_asset_2',
    css: 'transparent;',
    w:250,
    h:250,
    x:620,
    y:515,
    z:-233,
    rx:90,
    ry:-90,
    rz:0,
    game_id:2,
    access_code: '8',
    fun: function(){
        room_1_asset_2_function();
    }
};
var room_1_asset_2 = new Asset(room_1_asset_2_vars, cur_status);
room_1.add(room_1_asset_2);

var room_1_asset_3_function = function(v){
    handle_completed_asset_helper(1,3,3,v);
    if($('.lightbox-content .current').is('.slide_11,.slide_12,.slide_13,.slide_14')){    
        delegate_s11f_click();
    }
}
var room_1_asset_3_vars = {
    name:'room_1_asset_3',
    css: 'transparent;',
    w:437,
    h:478,
    x:620,
    y:217,
    z:-39,
    rx:90,
    ry:-90,
    rz:0,
    game_id:3,
    access_code: 'IV Infusion',
    fun: function(){
        room_1_asset_3_function();
    }
};
var room_1_asset_3 = new Asset(room_1_asset_3_vars, cur_status);
room_1.add(room_1_asset_3);

var room_1_asset_4_function = function(v){
    handle_completed_asset_helper(1,4,4,v);
    if($('.lightbox-content .current').is('.slide_15,.slide_16,.slide_17,.slide_18')){    
        delegate_s11f_click();
    }
}
var room_1_asset_4_vars = {
    name:'room_1_asset_4',
    css:'transparent;border-radius:50%;',
    w:156,
    h:152,
    x:335,
    y:630,
    z:62,
    rx:90,
    ry:0,
    rz:0,
    game_id:4,
    access_code: '20',
    fun: function(){
        room_1_asset_4_function();
    }
};
var room_1_asset_4 = new Asset(room_1_asset_4_vars, cur_status);
room_1.add(room_1_asset_4);

var room_1_asset_5_function = function(v){
    $('body').addClass('finished');
}
var room_1_asset_5_vars = {
    name:'room_1_asset_5',
    css:'transparent;border-radius:46% 46% 0 0;',
    w:548,
    h:674,
    x:-630,
    y:-278,
    z:-248,
    rx:270,
    ry:90,
    rz:180,
    access_code: 'Stelara Strong',
    fun: function(){
        room_1_asset_5_function();
    }
};
var room_1_asset_5 = new Asset(room_1_asset_5_vars, cur_status);
room_1.add(room_1_asset_5);

// End Activities

world.add_room(room_1);
viewport.camera.set_room(room_1);


//takes the drag item and centers it in correct place.
function snapToMiddle(dragger, target){
    if(target.find('.s6b-a').length === 0){
        dragger.detach().appendTo(target);
        dragger.css('top','0');
        dragger.css('left','0');
        //dragger.draggable('disable');
    }
}
//takes the drag item back to initial state.
function snapToStart(dragger, target){
    dragger.animate({top:0,left:0},{duration:600,easing:'easeOutBack'});
    dragger.css('top','0');
    dragger.css('left','0');
}
//every time drag is dropped, this runs to check if the user has selected every item.
function check_drag_complete(){
    var all_done = false;
    if($('.lightbox-content .current .d-table .s6b-a').length === 2){
        all_done = true;
    }
    if(all_done){
        $(".s6b-a").draggable({disabled: true});
        $('.s6b-d').droppable({disabled: true});
        $('.lightbox-content .current .submit').off(device_click_event);
        $('.lightbox-content .current .slide-content-wrap').removeClass('show').addClass('hide');
        //now lets check if it was correct
        var correct = true;
        $('.lightbox-content .current .d-table .s6b-a').each(function(){
            if($(this).data('unit') !== $(this).parent().data('unit')){
                correct = false;
                return false;
            }
        });
        if(correct){ //correct
            tally_score();
            $('.current .feedback').html(
                '<div class="display-table">'+
                    '<div class="display-cell">'+
                        '<div class="correct-msg">That is correct!</div>'+
                        '<div class="continue-btn btn">'+
                            '<div class="continue-btn-child btn-child">'+
                                'NEXT QUESTION'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        else{ //choise incorrect
            $('.current .feedback').html(
                '<div class="display-table">'+
                    '<div class="display-cell">'+
                        '<div class="incorrect-msg">Incorrect!</div>'+
                        '<div class="expl">The answers should be switched.</div>'+
                        '<div class="continue-btn btn">'+
                            '<div class="continue-btn-child btn-child">'+
                                'NEXT QUESTION'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        $('.lightbox-content .current .feedback').addClass('show').removeClass('hide');
        $('.lightbox-content .current').addClass('completed');
        slides_unlocked = true;
        add_prev_next();
    }
    all_done = null;
}

function handle_prev_next_helper(){
    stop_audio();
    $('video').trigger('pause');
    if(!$('.current').hasClass('completed')){
        slides_unlocked = false;
    }
    if($('.lightbox-content .current').is('.slide_1,.slide_3,.slide_19,.slide_4,.slide_5,.slide_6,.slide_7,.slide_8,.slide_9,.slide_10,.slide_11,.slide_12,.slide_13,.slide_14,.slide_15,.slide_16,.slide_17,.slide_18')){    
        delegate_s11f_click();
    }
    else if($('.lightbox-content .current').hasClass('slide_2')){
        $(".s6b-a").draggable({disabled: true});
        $('.s6b-d').droppable({disabled: true});
        $(".s6b-a").draggable( {
            opacity: .4,
            create: function(){
                $(this).data('position',$(this).position());
            },
            //cursorAt:{left:15},
            cursor:'move',
            revert: 'invalid',
            start:function(){
                $(this).stop(true,true);
            },
            stop: function(){
            },
            disabled:false
        });
        $('.s6b-d').droppable({
            over: function(event, ui) {
                $(ui.helper).unbind("mouseup");
            },
            drop:function(event, ui){
                snapToMiddle(ui.draggable,$(this));
            },
            out:function(event, ui){
                $(ui.helper).mouseup(function() {
                    snapToStart(ui.draggable,$(this)); 
                });
            },
            disabled:false
        });
        $('.lightbox-content .current .submit').off(device_click_event).on(device_click_event,function(){
            check_drag_complete();
        });
    }
    $(main_audio).off('timeupdate');
    add_prev_next();
    update_circles();
}

var handle_next_click = function(e){
    if($('.lightbox-content .next').hasClass('available')){
        var el = $('.current');
        if($('.lightbox-content .current').hasClass('image-slide')){
            slides_unlocked = true;
        }
        if(slides_unlocked && el.next('.slide').length > 0){
            cur_status.finished($('.lightbox-content').attr('name')+'_slide_'+($('.current').index()-1));
            el.addClass('before').removeClass('current');
            el.next('.slide').addClass('current').removeClass('before after');
            handle_prev_next_helper();
        }
        el = null;
    }
}
var handle_prev_click = function(e){
    if($('.lightbox-content .prev').hasClass('available')){
        var el = $('.current');
        if($('.lightbox-content .current').hasClass('image-slide') || 
            $('.lightbox-content .current').prev('.slide').hasClass('completed') ||
            $('.lightbox-content .current').prev('.slide').hasClass('image-slide')){
            slides_unlocked = true;
        }
        if(el.prev('.slide').length > 0){
            //cur_status.finished($('.lightbox-content').attr('name')+'_slide_'+($('.current').index()-3));
            el.addClass('after').removeClass('current');
            el.prev('.slide').addClass('current').removeClass('before after');
            handle_prev_next_helper();
        }
        el = null;
    }
};

$('.lightbox-content').off();   //remove previous delegated handlers.
function delegate_prev_next_events(){
    $('.lightbox-content').off(device_click_event);
    $('.lightbox-content').on(device_click_event,function(e){
        if(good_click(e)){
            var clicked_el = $(e.target);
            if(clicked_el.hasClass('flip')){
                $('.current .quickcard,.current.quickcard-slide').toggleClass('flipped');
                if(!clicked_el.closest('.slide').hasClass('completed')){
                    clicked_el.closest('.slide').addClass('completed');
                    slides_unlocked = true;
                    add_prev_next();
                    if($('.current').next('.slide').length == 0){
                        $('.lightbox').addClass('open');
                    }
                }
                $('.lightbox-content').off(device_click_event);
                setTimeout(function(){
                    delegate_prev_next_events();
                },750);
            }
            else if(clicked_el.hasClass('next')){
                handle_next_click();
            }
            else if(clicked_el.hasClass('prev')){
                handle_prev_click();
            }
            else if(clicked_el.hasClass('continue-btn-child') || clicked_el.hasClass('continue') || clicked_el.hasClass('continue2')){
                if($('.next').hasClass('available')){
                    $('.next').trigger(device_click_event);
                }
                else{
                    close_lightbox();
                }
            }
            else if(clicked_el.hasClass('handles_continue')){
                if($('.next').hasClass('available')){
                    $('.next').trigger(device_click_event);
                }
            }
            clicked_el = null;
        }
    });
}
delegate_prev_next_events();
//$('.page-title').text('Lesson 1');
//===============END room_1==============================