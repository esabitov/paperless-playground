/*
*
*   Author: Elliot Sabitov
*   Company: Gramercy Tech
*   Description: Global variables declarations
*
*
*/
var loop;
var gated = true;
var attempt = 0;
var maxSpeed;
var accel;
if(gated){
    maxSpeed = 5;
    accel = 0.2;
}
else{
    maxSpeed = 50;
    accel = 2;
}
var speed = 0;
var viewport;
var world;
var cur_status;
var init_assets;
var keyState = {
    forward: false,
    backward: false,
    strafeLeft: false,
    strafeRight: false
};
var slides_unlocked = false;
var pointer = { x: 0, y: 0 };
//var originalZoom = 0; //zoom turned off.
var sequence = false;
//lets get the sequence
$.getJSON("/assets/sequence.json", function(result){
	sequence = result;
});
//sets room 1 coords in beginning.
var position = new Triplet(-690,0,200);
var rotation = new Triplet(270,0,-90);

//var highlight_interval;
var main_loop_interval;

var rotate_right_interval;
var rotate_left_interval;
var rotate_right_gated_interval;
var rotate_left_gated_interval;
var rotating = false;

//global function names
var enter_room;
var enter_room_helper;
var enter_quickcards;
var display_learning_objectives;
var close_lightbox;
var lightbox_init;
var handle_checkbox_click;
var update_circles;
var add_circles;
var add_prev_next;
var activate_asset;
var turn_on_all_available_assets;
var turn_on_new_available_assets;
var get_available_rooms_assets;
var check_room_asset_allowed;
var open_doors;
var handle_video_slide;

var show_answers;

var cookie_present = false;

var handle_checkbox_click_event;

//AUDIO
//main audio is the audio for everything
var main_audio;
var play_audio;
var stop_audio;

var open_menu;
var close_menu;

var handle_completed_asset_helper;

var handle_question_click;

var device_click_event;
var init_timeout;