/*
*
*   Author: Elliot Sabitov
*   Company: Gramercy Tech
*   Description: File handles all of the room interactions, looking/moving around, handling cookies on load,
*       entering rooms on load, firing main loop
*
*
*/

//sets the camera coords to given coords. Used for turning around the room etc..
function move_view(x,y){
    //if(!gated){
        if(viewport.camera.rotation.x - (y- pointer.y)/2 > 255 && 
            viewport.camera.rotation.x - (y - pointer.y)/2 < 275){
            viewport.camera.rotation.x -= (y - pointer.y)/2;
        }
    //}
    viewport.camera.rotation.z += (x - pointer.x)/2;
    if(viewport.camera.rotation.z >= 360 || viewport.camera.rotation.z <= -360){
        viewport.camera.rotation.z %= 360;
    }
    pointer.x = x;
    pointer.y = y;
    setCookie('rotation_x',viewport.camera.rotation.x);
    setCookie('rotation_z',viewport.camera.rotation.z);
}
//nav events
//same as enter_room but also activates the asset by calling the appropriate asset function. 
enter_room_asset = function(room,asset,slide){
    if(check_room_asset_allowed(room,asset)){
        stop_audio();
        //again, this check is redundant because I plan to disable clicks. This is perhaps temporary.
        if(asset == 0 && slide == 0){
            if(cur_status.room_num == room){
                display_learning_objectives(room_1);
            }else{
                enter_room(room,function(){
                    display_learning_objectives(window['room_'+room]);
                });
            }
        }else{
            if(room !== 0){
                cur_status.finished('room_'+room+'_objectives');
            }
            if(cur_status.room_num == room){
                //call the appropriate asset function and pass to it the room, asset, and slide numbers.
                window['room_'+room+'_asset_'+asset+'_function']('room_'+room+'_asset_'+asset+'_slide_'+slide+'');
            }else{
                setCookie('enter_room_asset','true');
                //enter the room first
                enter_room(room,function(){
                    //then call the appropriate asset function with the correct slide.
                    window['room_'+room+'_asset_'+asset+'_function']('room_'+room+'_asset_'+asset+'_slide_'+slide+'');
                    deleteCookie('enter_room_asset');
                });
            }
        }
    }
}

enter_room_helper = function(room_num, callback){
    if($('body').hasClass('welcome-screen')){
        $('body').removeClass('welcome-screen');
    }
    if($('.welcome').is(":visible")){
        $('.terms-wrap').removeClass('welcome-screen');
        $('.welcome').fadeOut(450);
    }
    $('.loader').fadeIn(450);
    setTimeout(function(){
        remove_all_nodes(world.node);
        remove_all_nodes(document.getElementsByClassName('loaded_content')[0]);
        $.ajax({    //get the js
            url:"/assets/room_"+room_num+".js",
            dataType: 'script',
            timeout: 5000
        }).done(function(){
            $('.loader').fadeOut(450);
            if($('.main-menu-wrap').hasClass('open')){
                close_menu();
            }
            cur_status.room_num = room_num;
            setCookie('position_x',viewport.camera.position.x);
            setCookie('position_y',viewport.camera.position.y);
            setCookie('position_z',viewport.camera.position.z);
            setCookie('rotation_x',viewport.camera.rotation.x);
            setCookie('rotation_y',viewport.camera.rotation.y);
            setCookie('rotation_z',viewport.camera.rotation.z);
            setCookie('room',room_num);
            if(callback!==false){
                setTimeout(function(){callback();},250);
            }
            clearInterval(main_loop_interval);
            main_loop_interval = setTimeout(loop, 15);
            turn_on_all_available_assets();
            //turn off room_0 door if entered room 2 or 3, and rooms arent complete.
            if(room_num == 2 || room_num == 3){
                //if that room isnt complete.
                if(!is_room_completed(room_num)){
                    $('.world').off(device_click_event, '.room_0');
                }
            }
            open_doors();
            //highlight_interval = setInterval(function(){$('.highlight').toggleClass('hover');},750);
        }).fail(function(jqxhr,settings,exception){
            console.log(jqxhr);console.log(settings);console.log(exception);
        });
    },350);
}

//enters a room
enter_room = function(room_num,callback){
    stop_audio();
    //this function has to return false if the room_num is not available.
    //room_num is not available if all the assets in the previous rooms are not completed.
    //at the end of this function I am going to call a function that will turn on the appropriate assets.
    //if(check_room_asset_allowed(room_num)){
        //technically this check is useless, because I plan to disable click listeners and highlights for anything
        //thats not allowed, so it wouldnt even get to this point.
        //callback function is used by the enter_room_asset funtion to call asset function after room is entered.
        callback = typeof callback !== 'undefined' ?  callback : false;
        enter_room_helper(room_num,callback);
    //}
}

//check allowed was created to prevent the users from exiting a room by walking through one of the walls.
//since the walking functionality (gated) was disabled, this is not used unless it is reenabled again.
function check_allowed(val,xyz){
    if(!gated){
        return true;
    }
    var ret = false;
    if(xyz == 'x'){
        if(val > viewport.camera.room.allowed.min.x && val < viewport.camera.room.allowed.max.x){
            ret = true;
        }
    }
    else if(xyz == 'y'){
        if(val > viewport.camera.room.allowed.min.y && val < viewport.camera.room.allowed.max.y){
            ret = true;
        }
    }
    else if(xyz == 'z'){
        if(val > viewport.camera.room.allowed.min.z && val < viewport.camera.room.allowed.max.z){
            ret = true;
        }
    }
    return ret;
}

// loop
//main loop runs every 15ms and listens for forward/backward key movements (if ungated) and updates the camera with 
//with the appropriate values based on what the user is doing in the room (whether walking or just rotation around)
loop = function(){
    if (keyState.backward) {
        if (speed > -maxSpeed) speed -= accel;
    } else if (keyState.forward) {
        if (speed < maxSpeed) speed += accel;
    } else if (speed > 0) {
        speed = Math.max( speed - accel, 0);
    } else if (speed < 0) {
        speed = Math.max( speed + accel, 0);
    } else {
        speed = 0;
    }
    var xo = Math.sin(viewport.camera.rotation.z * 0.0174532925);
    var yo = Math.cos(viewport.camera.rotation.z * 0.0174532925);
    var zo = Math.sin(viewport.camera.rotation.x * 0.0174532925);
    if(check_allowed(viewport.camera.position.x - xo * speed,'x')){
        viewport.camera.position.x -= xo * speed;
        setCookie('position_x',viewport.camera.position.x);
    }
    if(check_allowed(viewport.camera.position.y - yo * speed,'y')){
        viewport.camera.position.y -= yo * speed;
        setCookie('position_y',viewport.camera.position.y);
    }
    viewport.camera.update();
    //console.log('rotation: ' + viewport.camera.rotation.x + " and " + viewport.camera.rotation.y + " and "+ viewport.camera.rotation.z);
    //console.log(viewport.camera.position.x + " and " + viewport.camera.position.y);
    main_loop_interval = setTimeout(loop, 15);
};
//line below checks if device is touch capable.
device_click_event = ('ontouchstart' in document.documentElement) || navigator.maxTouchPoints ? 'touchend' : 'click';
function onMouseMove(e) {
    //if mouse moves then change the device_click_event to click.
    clearTimeout(init_timeout);
    var is_ipad = navigator.userAgent.match(/iPad/i) != null;
    if(device_click_event == 'touchend' && !is_ipad){
        device_click_event = 'click';
    }
    document.removeEventListener("mousemove", onMouseMove);
    continue_rest_of_init();
}
function continue_no_mouse(){
    document.removeEventListener("mousemove", onMouseMove);
    continue_rest_of_init();
}

function continue_rest_of_init(){
    continue_init();
    menu_init();
    lightbox_init();
    $(document).keyup(function(e) {
        if (e.keyCode === 27) { //close lightbox on escape
            close_lightbox();
        }
    });
}

jQuery(function($) { //at the very begining I try and understand whether we want to use mouse or touch

    //first I am going to check if there is a cookie set for device_click_event
    if(getCookie('device_click_event') !== null){
        //if cookie is set for device_click_event then use that value.
        device_click_event = getCookie('device_click_event');
        continue_rest_of_init();
    }
    else{   //if no cookie set then its new load.
        //I set a 1 second listener for mouse movement.
        document.addEventListener("mousemove", onMouseMove);
        //if mouse move happens then the init_timeout gets canceled and continues with click event.
        //otherwise continue_no_mouse happens and everything continues with touch event.
        init_timeout = setTimeout(function(){
            continue_no_mouse();
        },1000);
    }
    
});

//continue init is every after we have figured out whether to use mouse or touch. If a mousemove event is detected
//within first second then it will use mouse, otherwise touch. This is only for devices that have both mouse and touch
function continue_init(){
    var keys_pressed = [];    
    $(document).bind("keydown", function(e) {
        //custom code here that listens if ASD is pressed all together, then all cookies will be deleted.
        if(e.keyCode == 65 || e.keyCode == 83 || e.keyCode == 68){
            keys_pressed.push(e.keyCode);
        }
        if(keys_pressed.indexOf(65) !== -1 && keys_pressed.indexOf(83) !== -1 && keys_pressed.indexOf(68) !== -1){
            deleteAllCookies();
            alert('All cookies cleared');
            keys_pressed = [];
        }
    });
    $('.btn.refresh').on(device_click_event,function(){
        deleteAllCookies();
        alert('All cookies cleared');
    })
    $(document).bind("keyup", function(e) {
        keys_pressed = [];
    });
    
    if(getCookie('device_click_event') === null){
        setCookie('device_click_event',device_click_event); 
    }
    main_audio = document.getElementById('main_audio')
    //create viewport and world
    viewport = new Viewport( document.body );
    world = new World( viewport );   
    
    //now I look through the cookie and set the correct rotations and positions based on where the user left off
    //this is only significant on refresh, it runs once every refresh.
    if(getCookie('position_x') !== null && getCookie('position_y') !== null && getCookie('position_z') !== null &&
        getCookie('rotation_x') !== null && getCookie('rotation_y') !== null && getCookie('rotation_z') !== null){
        position.x = getCookie('position_x');
        position.y = getCookie('position_y');
        position.z = getCookie('position_z');
        rotation.x = getCookie('rotation_x');
        rotation.y = getCookie('rotation_y');
        rotation.z = getCookie('rotation_z');
        cookie_present = true;
    }
    var current_room = -1;
    if(getCookie('room') !== null){
        current_room = getCookie('room');
    }
    var completed_cookie = [];
    if(getCookie('completed') !== null){
        var val = getCookie('completed');
        completed_cookie = val.split('^');
    }
    cur_status = new Status(position,rotation,current_room,completed_cookie);
    function close_welcome_video(){
        $('.welcome-video-wrap').fadeOut('250');
        setTimeout(function(){
            $('.welcome-video-wrap').remove();
        },250);
    }
    if(cur_status.room_num !== null && cur_status.room_num !== -1){    //this will only happen onload means user has cookies
        $('.loader').fadeIn(450);
        $('body').removeClass('welcome-screen');
        $('.welcome').remove();
        enter_room(cur_status.room_num,function(){
            if(cookie_present){
                viewport.camera.position.x = position.x;
                viewport.camera.position.y = position.y;
                viewport.camera.position.z = position.z;
                viewport.camera.rotation.x = rotation.x;
                viewport.camera.rotation.y = rotation.y;
                viewport.camera.rotation.z = rotation.z;
                viewport.camera.update();
            } 
        });
    }
    else{   //no cookies, new user
        //play video
        /*var entry_vid = document.getElementById('welcome-video');
        if (entry_vid != null && entry_vid.value == '') {
            entry_vid.load();
            entry_vid.play();
        }*/
        //entry_vid.addEventListener('ended',close_welcome_video,false);
        //bind the begin button to enter the hallway
        /*$('.welcome-begin').bind(device_click_event, function(e){
            if(good_click(e)){
                $('.loader').fadeIn(450);
                setTimeout(function(){
                    $('body').removeClass('welcome-screen');
                    $('.welcome').remove();
                    enter_room(2);
                },450);            
            }
        });*/
        enter_room(1);
    }
    

    //#######################LISTENERS###########################
    $('#viewport').bind("touchstart", function(e) {
        if(e.originalEvent.touches.length <= 1){
            pointer.x = e.originalEvent.targetTouches[0].pageX;
            pointer.y = e.originalEvent.targetTouches[0].pageY;
            e.preventDefault();
        }
        /* zoom turned off
        else if(e.originalEvent.touches.length > 1){
            originalZoom = getScalingDistance(e.originalEvent.touches[0], e.originalEvent.touches[1]);
        }
        */
    });

    //#######################MOUSE ZOOM EVENTS###########################
    /* Removed zoom on 20160218
    var getScalingDistance = function(touches0, touches1){
        var dx = touches0.pageX - touches1.pageX,
            dy = touches0.pageY - touches1.pageY;
        return Math.sqrt(dx*dx + dy*dy) / 100;
    };
    $(document).bind('mousewheel', function(e){
        e.preventDefault();
        if(e.originalEvent.wheelDelta /120 > 0) {
            zoom_in();
        }
        else if(e.originalEvent.wheelDelta / 120 < 0){
            zoom_out();
        }
    });
    $(document).bind('touchmove', function(e){
        if (e.originalEvent.touches.length > 1){
            if(originalZoom < getScalingDistance(e.originalEvent.touches[0], e.originalEvent.touches[1])){
                zoom_in();
            }
            else{
                zoom_out();
            }
        }
    });
    function zoom_in(){
        if(viewport.camera.fov+10 < (viewport.camera.room.fov * 1.5)){
            viewport.camera.fov += 10;
        }
        viewport.camera.update();
    }
    function zoom_out(){
        if(viewport.camera.fov-10 > viewport.camera.room.fov){
            viewport.camera.fov -= 10;
        }
        viewport.camera.update();
    }
    */
    //#######################END MOUSE ZOOM EVENTS###########################
    $('#viewport').bind("touchmove", function(e){
        if (e.originalEvent.touches.length <= 1){
            //if ((!$('.obj').is(e.target) && $('.obj').has(e.target).length === 0)){
                var ev = e.originalEvent;
                move_view(ev.targetTouches[0].pageX,ev.targetTouches[0].pageY);
                ev.preventDefault();
            //}
        }
    });
    if(!gated){
        $('#viewport').bind("mouseover", function(ev) {
            pointer.x = ev.pageX;
            pointer.y = ev.pageY;   
            document.removeEventListener("mouseover", arguments.callee)
        });
        $('#viewport').bind("mousedown", function(ev) {
            ev.preventDefault()
            pointer.x = ev.pageX;
            pointer.y = ev.pageY;
            $(document).bind("mousemove", function(e){
                //if ((!$('.obj').is(e.target) && $('.obj').has(e.target).length === 0)){
                    move_view(e.pageX,e.pageY);
                //}
            });
        });
    }
    else{
        /*
        var startX;
        var startY;
        var dist;
        $('#viewport').bind("touchstart", function(ev){
            var e = ev.originalEvent;
            if (e.touches.length <= 1){
                //if ((!$('.obj').is(e.target) && $('.obj').has(e.target).length === 0)){
                    dist = 0;
                    startX = e.changedTouches[0].pageX;
                    e.preventDefault();
                //}
            }
        });
        $('#viewport').bind("touchmove", function(ev){
            var e = ev.originalEvent;
            if (e.touches.length <= 1){
                //if ((!$('.obj').is(e.target) && $('.obj').has(e.target).length === 0)){
                    e.preventDefault();
                //}
            }
        });
        $('#viewport').bind("touchend", function(ev){
            var e = ev.originalEvent;
            dist = e.changedTouches[0].pageX - startX;
            if(dist>0 && !rotating){
                rotate_left_fun();
            }
            else if(dist<0 && !rotating){
                rotate_right_fun();
            }
            e.preventDefault();
        }); 
        */  
    }

    $('#viewport').bind("mouseup", function(ev) {
        $(document).off("mousemove");
    });    
    
    if(!gated){
        $(document).bind("keydown", function(e) {
            switch (e.keyCode) {
                case 87:
                    keyState.forward = true;
                    break;  
                case 83:
                    keyState.backward = true;
                    break;  
            }
        });

        $(document).bind("keyup", function(e) {
            switch (e.keyCode) {
                case 87:
                    keyState.forward = false;
                    break;  
                case 83:
                    keyState.backward = false;
                    break;
            }
        });
    }

    $(document).bind('mouseup touchend', function(e){
        keyState.backward = false;
        keyState.forward = false;
        clearInterval(rotate_right_interval);
        clearInterval(rotate_left_interval);
        //e.preventDefault();
    });

    //based on cookies and sequence below I turn on doors and assets that need to be turned on. 
    open_doors();
    turn_on_all_available_assets();

    //initiallize a highlight interval.
    //highlight_interval = setInterval(function(){$('.highlight').toggleClass('hover');},750);

    //loop();

}