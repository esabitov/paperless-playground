/*
*
*   Author: Elliot Sabitov
*   Company: Gramercy Tech
*   Description: Code for handling top menu listeneres, navigation, and menus.
*
*
*/
var rotate_left = function(){
    viewport.camera.rotation.z -= 2;
};

var rotate_right = function(){
    viewport.camera.rotation.z += 2;
};

//helper for rotating right
function rotate_right_help(original){
    if(viewport.camera.rotation.z + 1 <= original + 45){
        viewport.camera.rotation.z += 1;
    }
    else{
        if(viewport.camera.rotation.z >= 360 || viewport.camera.rotation.z <= -360){
            viewport.camera.rotation.z %= 360;
        }
        clearInterval(rotate_right_gated_interval);
        rotating = false;
        setCookie('rotation_x',viewport.camera.rotation.x);
        setCookie('rotation_z',viewport.camera.rotation.z);
    }
}
//helper for rotating left
function rotate_left_help(original){
    if(viewport.camera.rotation.z - 1 >= original - 45){
        viewport.camera.rotation.z -= 1;
    }
    else{
        if(viewport.camera.rotation.z >= 360 || viewport.camera.rotation.z <= -360){
            viewport.camera.rotation.z %= 360;
        }
        clearInterval(rotate_left_gated_interval);
        rotating = false;
        setCookie('rotation_x',viewport.camera.rotation.x);
        setCookie('rotation_z',viewport.camera.rotation.z);
    }
}
//rotates the user to the right
function rotate_right_fun(){
    rotating = true;
    var original = viewport.camera.rotation.z;
    rotate_right_gated_interval = setInterval(function(){
        rotate_right_help(original);
    }, 10);

}
//rotates the user to the left
function rotate_left_fun(){
    rotating = true;
    var original = viewport.camera.rotation.z;
    rotate_left_gated_interval = setInterval(function(){
        rotate_left_help(original);
    }, 10);
}
function menu_init(){
    //delegate the turn right nav button
    $('.nav .btn.right').bind(device_click_event, function(e){
        if(good_click(e)){
            if(!gated){
                rotate_right_interval = setInterval(rotate_right, 30);
            }
            else{
                if(!rotating){
                    rotate_right_fun();
                }
            }
            e.preventDefault();
        }
    });
    //delegate the turn left nav button
    //$('[data-icon="d"]').bind(device_click_event, function(e){
    $('.nav .btn.left').bind(device_click_event, function(e){
        if(good_click(e)){
            if(!gated){
                rotate_left_interval = setInterval(rotate_left, 30);
            }
            else{
                if(!rotating){
                    rotate_left_fun();
                }
            }
            e.preventDefault();
        }
    });
}