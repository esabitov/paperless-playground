/*
*
*   Author: Elliot Sabitov
*   Company: Gramercy Tech
*   Description: Global scope functions, classes, and css utility functions that are used to make the rooms, cookie functions.
*
*
*/

/* CSS Utility Functions
returns css for 3d rendering of divs/planes
-------------------------------------------------- */
var CssUtils = (function() {    
    var s = document.documentElement.style;
    var vendorPrefix = 
        (s.WebkitTransform !== undefined && "-webkit-") ||
        (s.MozTransform !== undefined && "-moz-") ||
        (s.msTransform !== undefined && "-ms-") || "";
    
    return {
        translate: function( x, y, z, rx, ry, rz ) {
            //css for 3d transformations
            return vendorPrefix + "transform:" +
                "translate3d(" + x + "px," + y + "px," + z + "px)" +    
                "rotateX(" + rx + "deg)" +
                "rotateY("  +ry + "deg)" +
                "rotateZ(" + rz + "deg);"
        },
        origin: function( x, y, z ) {
            return vendorPrefix + "transform-origin:" + x + "px " + y + "px " + z + "px;";
        },
        texture: function( colour, rx, ry, rz ) {
            //css for background
            var a = Math.abs(-0.5+ry/180)/1.5;
            if (rz!==0) {
                a/=1.75;
            }
            //return "background:"+vendorPrefix +"linear-gradient(rgba(0,0,0," + a + "),rgba(0,0,0," + a + "))," + colour + ";";
            return "background:"+colour+";";
        }       
    }
}());


/* Triplet
-------------------------------------------------- */

function Triplet( x, y, z ) {
    //simple xyz object.
    this.x = x || 0;
    this.y = y || 0;
    this.z = z || 0;
}

/* Camera
-------------------------------------------------- */

function Camera( world, x, y, z, rx, ry, rz) {
    this.world = world;
    this.position = new Triplet(x, y, z);
    this.rotation = new Triplet(rx, ry, rz);    
    this.fov = 700;
    this.room = null;
}

Camera.prototype = {
    update: function() {
        //updates css to the new position and roation coordinates
        if (this.world) {
            this.world.node.style.cssText=
                CssUtils.origin( -this.position.x, -this.position.y, -this.position.z) +
                CssUtils.translate( this.position.x, this.position.y, this.fov, this.rotation.x, this.rotation.y, this.rotation.z)
        }
    },
    set_room: function(room){
        //sets camera to predefined room properties/coordinates.
        //updates cookies
        this.position.x = room.position.x;
        this.position.y = room.position.y;
        this.position.z = room.position.z;
        this.rotation.x = room.rotation.x;
        this.rotation.y = room.rotation.y;
        this.rotation.z = room.rotation.z;
        setCookie('position_x',this.position.x);
        setCookie('position_y',this.position.y);
        setCookie('rotation_x',this.rotation.x);
        setCookie('rotation_z',this.rotation.z);
        this.fov = room.fov;
        this.room = room;
        this.update();
    },
    pan_around_room: function(room){
        //takes in a room and turns left 90 degress 
        //used upon entry into hallway.
        room = this.room;
        var camera = this;
        var original = room.rotation;
        var pan_around_room = setInterval(function(){
            if(camera.rotation.z - 2 >= original.z - 90){
                camera.rotation.z -= 2;
                camera.update();
            }else{
                room.rotation.z = camera.rotation.z;
                setCookie('rotation_z',camera.rotation.z);
                clearInterval(pan_around_room);
            }
        }, 30);
    },
    pan_to_location: function(z){   //pan_to_location takes in a z rotation value (in degress) and rotates camera there
        room = this.room;
        var camera = this;
        var original = this.rotation;
        //first we should make original and incoming z to both be positive or negative.
        if(original.z > 0 && z < 0){
            z = 360+z;
        }
        else if(original.z < 0 && z > 0){
            z = z-360;
        }  
        //now that they are the same sign, take shortest path there. 
        var pan_around_room = setInterval(function(){
            if(original.z >= z){
                if(camera.rotation.z - 1 >= z){
                    camera.rotation.z -= 1;
                    camera.update();
                }else{
                    room.rotation.z = camera.rotation.z;
                    setCookie('rotation_z',camera.rotation.z);
                    clearInterval(pan_around_room);
                }
            }
            else if(original.z <= z){
                if(camera.rotation.z + 1 <= z){
                    camera.rotation.z += 1;
                    camera.update();
                }else{
                    room.rotation.z = camera.rotation.z;
                    setCookie('rotation_z',camera.rotation.z);
                    clearInterval(pan_around_room);
                }
            }
            //if equual then do nothing.
        }, 20);
    },
    set_status: function(status){
        //sets camera to the status properties/coodrinates.
        this.position.x = status.x;
        this.position.y = status.y;
        this.position.z = status.z;
        this.rotation.x = status.rx;
        this.rotation.y = status.ry;
        this.rotation.z = status.rz;
        setCookie('position_x',this.position.x);
        setCookie('position_y',this.position.y);
        setCookie('rotation_x',this.rotation.x);
        setCookie('rotation_z',this.rotation.z);
        this.update();
    },
    reset: function(){
        //reset no longer used but was previously activated by a reset button that would set camera back to intial room coordinates.
        if (this.world && this.room) {
            this.set_room(this.room);
        }
    },
}

/* Plane
-------------------------------------------------- */
//this can/should be made into plygons
function Plane(colour,w,h,x,y,z,rx,ry,rz,myclass,content,id) {
    this.node = document.createElement("div")
    this.node.className="plane";
    if(myclass){
        this.node.className+= " "+myclass;
    }
    if(id){
        this.node.id = id;
    }
    if(content){
        this.node.innerHTML = '<div class="plane-content">'+content+'</div>';
    }
    this.colour = colour;
    this.width = w;
    this.height = h;
    this.position = new Triplet(x, y, z);
    this.rotation = new Triplet(rx, ry, rz);
    this.update();
}

Plane.prototype = {
    update: function() {
        this.node.style.cssText += 
            "width:" + this.width + "px;" +
            "height:" + this.height + "px;" +
            CssUtils.texture(this.colour, this.rotation.x, this.rotation.y, this.rotation.z) +
            CssUtils.translate( this.position.x, this.position.y, this.position.z, this.rotation.x, this.rotation.y, this.rotation.z)
    }
}

/* World
-------------------------------------------------- */

function World( viewport ) {
    this.node = document.createElement("div")
    this.node.className = "world"
    viewport.node.appendChild(this.node)
    viewport.camera.world = this;
}

World.prototype = {
    add_room: function(room){
        for(var i=0;i<room.objects.length;i++){
            this.node.appendChild(room.objects[i].node);
        }
    },
    add_single: function(obj){
        this.node.appendChild(obj.node);
    },
}

/* Viewport
-------------------------------------------------- */

function Viewport( node ) {
    this.node = document.createElement("div")
    this.node.className = "viewport"
    this.node.id = "viewport"
    this.camera = new Camera()
    node.appendChild(this.node)
}

/* Room
-------------------------------------------------- */

function Room(id,x,y,z,rx,ry,rz,ax_min,ay_min,az_min,ax_max,ay_max,az_max){
    this.id = id;
    this.fov = 700;
    this.position = new Triplet(x, y, z);
    this.rotation = new Triplet(rx, ry, rz);
    this.allowed = {
        min : new Triplet(ax_min,ay_min,az_min),
        max : new Triplet(ax_max,ay_max,az_max)
    };
    this.objects = [];
    this.learningobjectives = '';
    this.viewed = false;
}

Room.prototype = {
    add: function(inc){
        if(inc instanceof Asset){
            this.objects.push(inc.obj);
        }
        else{
            this.objects.push(inc);
        }
    },
    set_learning_objectives: function(html){
        this.learningobjectives = html;
    },
    get_learning_objectives: function(){
        return this.learningobjectives;
    }
}

/* Status
-------------------------------------------------- */
function Status(position,rotation,room_num,completed){
    this.x = position.x;
    this.y = position.y;
    this.z = position.z;
    this.rx = rotation.x;
    this.ry = rotation.y;
    this.rz = rotation.z;
    this.room_num = room_num;
    this.completed = completed;
    this.cur = '';
}

Status.prototype = {
    getPoition : function(){
    //returns position triplet.
        return this.position;
    },
    finished : function(v){
    //function gets v which would be a unique identifier for an asset/activity so that it can be stored in status and cookie.
    //example for v: room_1_asset_2_slide_2.
        var asset_completed = false;
        if(this.completed.indexOf(v) == -1){
            //check that it doesnt have "slide" if it does then overwrite with higher slide number
            var in_array = value_in_array(this.completed,v);
            if(v.indexOf('_slide_') !== -1){
                //check if this asset has not been completed yet. Do nothing if it has.
                if(this.completed.indexOf(v.substring(0,v.indexOf('_slide_'))) == -1){
                    //check if room_x_asset_x_slide_x is in array, if it is then overwrite, else add to array
                    if(in_array !== false){
                        var new_val = parseInt(v.substr(v.indexOf('slide_')+6));
                        var old_val = parseInt(this.completed[in_array].substr(this.completed[in_array].indexOf('slide_')+6));
                        if(old_val<new_val){
                            this.completed[in_array] = v;
                        }
                    }else{
                        this.completed.push(v);
                    }
                }
            }
            else if(v.indexOf('room_') !== -1 && v.indexOf('_asset_') !== -1 && v.indexOf('_slide_') == -1){
                //loop thoruhg array and see if you can find "room_x_asset_x_slide_x" because if room_x_asset_x is present, then slide can be removed
                var found = false;
                var i;
                for(i=0; i<this.completed.length; i++){
                    if(this.completed[i].indexOf(v) !== -1){
                        found = true;
                        break;
                    }
                }
                if(found){
                    this.completed[i] = v;
                }
                else{
                    this.completed.push(v);
                }
                asset_completed = true;
            }
            else{
                this.completed.push(v);
            }
            var val = this.completed.join("^");
            setCookie('completed',val);
            if(asset_completed){
                //Lets run the ajax here
                var game_id = window[$('.lightbox-content').attr('name')].game_id;
                var game_score = getCookie('score_game_'+game_id);
                this.update_db(getCookie('user_id'),game_id,game_score);
                game_id = null;
                game_score = null;
                //also lets deactivate asset so that it cannot be accessed again
                $('.world').off(device_click_event,'.' + $('.lightbox-content').attr('name'));                
                $('.'+$('.lightbox-content').attr('name')).addClass('asset_complete');
                //OVER HERE IT MEANS THAT THE ASSET WAS COMPLETED SUCCESSFULLY SO I CAN RUN turn_on_new_available_assets()
                //to update the assets and make new ones available/highlighted.
                //I am also going to run open_doors() so that if the entire room is completed, then the doors can be updated.
                //open doors will pretty much never do anything unless they really did just finish a room.
                turn_on_new_available_assets();
                open_doors();
            }
        }
        //also store slide as current
        if(v.indexOf('_slide_') !== -1){
            this.cur = v;
        }
        setCookie('cur',this.cur);
    },
    update_db : function(user_id, game_id, points){
        $.ajax({ //set in db
            url:"/scores/set",
            data: { user_id : user_id, game_id : game_id, points: points },
            method: "POST",
            timeout: 5000
        }).done(function(data){
            console.log(data);
        }).fail(function(jqxhr,settings,exception){
            console.log(jqxhr);console.log(settings);console.log(exception);
        });
    },
    isComplete : function(name){
        //f() takes in a unique asset identifier (room_1_asset_2_slide_3) and returns whether or not that asset/activity has been completed.
        if(this.completed.indexOf(name) > -1){
            return true;
        }
        else{
            return false;
        }
    }
}

/*
Name: value_in_array
Description: Returns the index of v in arr
*/
function value_in_array(arr,v){
    var found = false;
    var i;
    for(i=0; i<arr.length; i++){
        if(arr[i].indexOf(v.substr(0, v.indexOf('_slide_'))) !== -1){
            found = true;
            return i;
        }
    }
    return false;
}

/* Asset
-------------------------------------------------- */
/*
An asset is the medium through which the user can access the activity. The asset is tied to a plane, which is the visual div
that acts as a button to begin an activity. There are also other properties tied to the asset object, including:
 (str) a unique name,
 (bool) whether or not the asset has been completed, 
 (str) html that can be passed to the plane, 
 (str) classes (cls) for the plane div [css string],
 (function) a function that is the js code to make the plane active for user interaction. Js code that runs the lightbox
    content for the particular activity. 
*/
function delegate_asset_code_lightbox_close_button(){
    $('.lightbox .close').on(device_click_event, function(e){
        if(good_click(e) && $('.lightbox').hasClass('open')){
            //close lightbox.
            $('.lightbox').fadeOut(300);
            $('.lightbox').removeClass('open');
            $('.lightbox-content').html('');
            lightbox_init();
            main_loop_interval = setTimeout( loop, 15);
        }
    });
}

function turn_on_as(vars,obj){
    $('.world').on(device_click_event,'.'+obj.name,{asset_name:obj.name},function(e){
    if(good_click(e)){
        if(vars.access_code && (getCookie(e.data.asset_name+'_accessed') === null)){
            obj.access_code = vars.access_code;
            var access_code_request_html = 
                '<div class="slides">'+
                    '<div class="slide display-table current a-code">'+
                        '<div class="close dark"></div>'+
                        '<div class="display-cell">'+
                            '<img src="/assets/access_code.png"/>'+
                            '<input type="text" class="access-code" />'+
                            '<div class="btn okay">'+
                                '<div class="btn-child">'+
                                    'ENTER'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
            //var access_code_request_html = '<div class="confirm-outer-wrap a-code"><div class="okay-overlay"></div><div class="confirm-inner-table"><div class="confirm-inner-cell"><div class="confirm-inner"><div class="close dark"></div></div></div></div></div>';
            $('.lightbox-content').html(access_code_request_html);
            $('.lightbox .close').off(device_click_event);
            delegate_asset_code_lightbox_close_button();
            $('.lightbox-content .btn.okay .btn-child').off(device_click_event).on(device_click_event,{asset_name:e.data.asset_name},function(e){
                $('.lightbox-content .btn.okay .btn-child').off(device_click_event);
                function grantAccess(asset_name) {
                    //set cookie so that they dont get prompted to enter access token again
                    setCookie((asset_name+'_accessed'),'true');
                    $('.lightbox').fadeOut(300);
                    $('.lightbox').removeClass('open');
                    $('.lightbox-content').html('');
                    lightbox_init();
                    setTimeout(function(){
                        vars.fun();
                    },400);
                }
                //check if access_code is valid                            
                if($('.lightbox-content .access-code').val().toLowerCase() == vars.access_code.toLowerCase()){
                    grantAccess(e.data.asset_name);
                }
                else{
                    // alert('Incorrect access code.');
                    $('.lightbox-content .slide .display-cell').append(
                        '<div class="ac-feedback">'+
                            'That is an incorrect access code.<br/>Please try again.'+
                        '</div>');
                    delegate_asset_code_lightbox_close_button();
                    $('.lightbox-content .access-code, .lightbox-content .btn.okay .btn-child').off(device_click_event).on(device_click_event,{asset_name:e.data.asset_name},function(e){
                        $('.ac-feedback').remove();
                        delegate_asset_code_lightbox_close_button();
                        $('.lightbox-content .btn.okay .btn-child').on(device_click_event,{asset_name:e.data.asset_name},function(e){
                            grantAccess(e.data.asset_name);
                        });
                    });
                }
                this.removeEventListener('ended', arguments.callee);
            });
            lightbox_show();
        }
        else{
            vars.fun();
        }
    }
});
}

function Asset(vars, cur_status){
    this.name = vars.name;
    this.cls = '';
    if(vars.cls){
        this.cls = vars.cls;
    }
    this.cls += ' ' + vars.name;
    this.html = vars.html||null;
    this.complete = 0;
    if(cur_status.isComplete(this.name)){
        this.complete = 1;
    }
    this.obj = new Plane(vars.css,vars.w,vars.h,vars.x,vars.y,vars.z,vars.rx,vars.ry,vars.rz,'asset '+this.cls,this.html,this.name);
    this.varsfun = vars.fun;
    this.activated = false;
    this.score = 1000;
    if(vars.score){
        this.score = vars.score;
    }
    this.game_id = 1;
    if(vars.game_id){
        this.game_id = vars.game_id;
    }
    if(vars.fun){
        this.fun = function(){
            //check to see if there is a timestamp
            var available = true;
            if(vars.timestamp){
                if(Math.floor(Date.now() / 1000) < vars.timestamp){
                    available = false;
                }
            }
            if(available){
                //if asset not in completed cookie
                var completed = getCookie('completed');
                if(completed !== null){
                    completed = completed.split('^');
                    if(completed.length){
                        if(completed.indexOf(this.name) == -1){
                            turn_on_as(vars,this)
                        }
                    }
                    else{
                        turn_on_as(vars,this)
                    }
                }
                else{
                    turn_on_as(vars,this)
                }
            }
        }
        //I need something here to prevent this from initializing if the asset is locked.
        //an asset would be locked if the previous required assets have not all been completed.
        /*
        this.fun();
        if(this.complete == 0){
            this.cls += ' highlight';
        }
        then in my functions I can activate an asset by running something like {
            activate_asset(2,1);
        }
        */
    }
}
/*
rotate_to_asset - rotates to the location provided and will rotate to that location when the appropriate
    asset has been completed. Only used within activate_asset()
*/
function rotate_to_asset(room_num,need_room_num,asset_num,need_asset_num,location){
    if(getCookie('first_time_rotate_room_'+need_room_num+'_asset_'+need_asset_num) === null || getCookie('first_time_rotate_room_'+need_room_num+'_asset_'+need_asset_num) === ''){
        if(room_num == need_room_num && asset_num == need_asset_num){
            viewport.camera.pan_to_location(location);
            setCookie('first_time_rotate_room_'+need_room_num+'_asset_'+need_asset_num,'true');
        }
    }
}

function activate(room_num, asset_num){
    if(window['room_'+room_num+'_asset_'+asset_num].activated == false && 
        ! $('.room_'+room_num+'_asset_'+asset_num).hasClass('asset_complete')){
        window['room_'+room_num+'_asset_'+asset_num].fun();
        window['room_'+room_num+'_asset_'+asset_num].obj.node.className += ' highlight';
        window['room_'+room_num+'_asset_'+asset_num].activated = true;
    }
}

/*
activate_asset: gets the room and asset numbers and adds 'highlight' class to the appropriate asset plane div, and makes 
    that asset activated. This function runs every time an asset is completed.
*/
activate_asset = function(room_num, asset_num){
    if(typeof room_num == 'undefined' || typeof asset_num == 'undefined'){
        return;
    }
    else{
        if(typeof window['room_'+room_num+'_asset_'+asset_num] !== 'undefined' && window['room_'+room_num+'_asset_'+asset_num] !== null){
            if(getCookie('completed') === null){
                activate(room_num, asset_num);
            }
            else{
                var completed = getCookie('completed');
                completed = completed.split('^');
                if(completed.length){
                    if(completed.indexOf('room_'+room_num+'_asset_'+asset_num) == -1){
                        activate(room_num, asset_num);
                    }
                }
                else{
                    activate(room_num, asset_num);
                }
            }
            //rotate_to_asset(room_num,1,asset_num,3,-270);
        }
    }
}

//Helper function for turn_on_room_helper
function turn_on_asset_helper(available_room_num,assets){
    if($.isArray(assets)){
        $.each(assets,function(index,asset){
            if($.isArray(asset)){
                $.each(asset,function(i,v){
                    var available_asset_num = parseInt(v.slice(-1));
                    activate_asset(available_room_num,available_asset_num);
                });
            }
            else{
                var available_asset_num = parseInt(asset.slice(-1));
                activate_asset(available_room_num,available_asset_num);
            }
        });
    }
    else{
        var available_asset_num = parseInt(assets.slice(-1));
        activate_asset(available_room_num,available_asset_num);
    }
}

//Helper function for turn_on_new_available_assets
function turn_on_room_helper(room_or_rooms){
    $.each(room_or_rooms,function(room_name,assets){
        var available_room_num = parseInt(room_name.slice(-1));
        turn_on_asset_helper(available_room_num,assets);
    });
}
//Turns on assets that the user is able to now access. All depends on cookie and where they are in sequence.
turn_on_new_available_assets = function(){
    var available_sequence = get_next_available_rooms_assets();
    var latest = available_sequence[available_sequence.length - 1];
    if($.isArray(latest)){
        $.each(latest,function(room_index,room_object){
            turn_on_room_helper(room_object);
        });
    }
    else{
        turn_on_room_helper(latest);
    }
}

//Turns on all asserts that are available for the user.
turn_on_all_available_assets = function(){
    var available_sequence = get_available_rooms_assets();
    $.each(available_sequence,function(i,room_or_rooms){
        if($.isArray(room_or_rooms)){
            $.each(room_or_rooms,function(room_index,one_room){
                turn_on_room_helper(one_room);
            });
        }
        else{
            turn_on_room_helper(room_or_rooms);
        }
    });
}

function is_asset_completed(asset_name,completed){
    //checks the asset name against the completed array and returns boolean of whether or not the 
    //asset_name is in the array. If true then asset was compeleted
    if($.inArray(asset_name,completed) == -1){
        return false;
    }
    else{
        return true;
    }
}

//checks if all assets in a room have been completed, based on cookies and sequence
function is_room_completed(room_num){
    if(room_num == -1){
        return false;
    }
    var completed = getCookie('completed');
    if(completed !== null && completed !== ''){
        completed = completed.split('^');
        for(var i in sequence){
            var room = sequence[i];
            for(var room_name in room){
                if(room_name === 'room_'+room_num){
                    var room_obj = room[room_name];
                    for(var asset_index in room_obj){
                        asset_obj = room_obj[asset_index];
                        if(asset_obj.constructor === Array){
                            for(var asset in asset_obj){
                                asset_name = asset_obj[asset];
                                if(completed.indexOf(asset_name) === -1){
                                    return false;
                                }
                            }
                        }else{
                            asset_name = asset_obj;
                            if(completed.indexOf(asset_name) === -1){
                                return false;
                            }
                        }
                    }
                    return true;
                }
            }
        }
    }
    return false;
}

//helper function for get_next_available_rooms_assets
function get_next_available_rooms_assets_helper(ret,room_index_in_sequence_object,room,completed,multiroom){
    multiroom = typeof multiroom !== 'undefined' ? multiroom : false;
    var found = false;
    var temp = {};
    $.each(room,function(room_name,asset_arr){
        //I am looping here but this will really just be a one step loop that will access the index
        //and the value in the room. Index would be something like "room_1" and value would be the 
        //the array of assets.
        var f1 = false;
        $.each(asset_arr,function(i,asset){
            //finally loop through asset array
            var f2 = false;
            if(!multiroom){
                if(typeof ret[room_index_in_sequence_object] == 'undefined'){
                    ret[room_index_in_sequence_object] = {};
                }
                if(typeof ret[room_index_in_sequence_object][room_name] == 'undefined'){
                    ret[room_index_in_sequence_object][room_name] = [];
                }
            }
            if($.isArray(asset)){ 
                //asset_name is expected to be a string, BUT, if it is an array, then it means that 
                //multiple assets are available to the user at the same time.
                //so I have to loop through each one to see if user already completed all of them
                if(!multiroom){
                    ret[room_index_in_sequence_object][room_name] = [];
                }             
                $.each(asset,function(x,asset_name){
                    if(!is_asset_completed(asset_name,completed)){
                        if(!multiroom){
                            ret[room_index_in_sequence_object][room_name] = asset;
                        }
                        else{
                            temp[room_name] = asset;
                        }
                        f2 = true;
                        found = true;
                        return false;
                    }
                });
            }
            else{
                //see if the user completed the asset   
                if(!multiroom){
                    ret[room_index_in_sequence_object][room_name] = asset;
                }             
                else{
                    temp[room_name] = asset;
                }
                if(!is_asset_completed(asset,completed)){
                    found = true;
                    f2 = true;
                }
            }
            if(f2){
                f1 = true;
                return false;
            }
        });
        if(multiroom){
            ret = temp;
        }
        if(f1){
            return false;
        }
    });
    //if it reaches this point, that means the user completed the assets in this room.
    //I am going to return false if room is completed, this way it can just continue on.
    return {ret:ret,found:found};  
}

//IMPORANT: I have a single function just to return one upcoming item that is available, ie  the newest available 
//assets are returned. The non single function returns evertything. The single may be useful for just activating
//assets one by one after ligthbox activity was completed.
get_next_available_rooms_assets = function(){
    var completed = getCookie('completed');
    var ret = [];
    $.each(sequence[0],function(room_name,asset_arr){
        var temp = {};
        temp[room_name] = [asset_arr[0]];
        ret.push(temp);
    });
    if(completed !== null){
        ret = [];
        completed = completed.split('^');
        //loop through the sequence json object and figure out what point the user is at by
        //comparing the sequence to the completed cookie array.
        //as soon as an asset is reached that is not in the completed array, the loop is broken,
        //and that point in the sequence is returned.
        //I return room number and the next available asset(s).
        $.each(sequence,function(n,room_or_rooms){
            var f1 = false;
            if($.isArray(room_or_rooms)){
                //this means multiple rooms are available together because in json multiple rooms in one array
                var ret_arr = [];
                $.each(room_or_rooms,function(x,room){
                    //so I have to loop through each room
                    /*
                    var val = get_available_rooms_assets_helper(ret,n,room,completed);
                    if(val !== false){
                        ret_arr.push(val);
                    }
                    */
                    var val = get_next_available_rooms_assets_helper([],n,room,completed,true);
                    var found = val.found;
                    ret_arr.push(val.ret);
                    if(found){
                        f1 = true;
                    }
                });
                if(ret_arr.length > 0){
                    ret.push(ret_arr);
                }
            }
            else{
                var val = get_next_available_rooms_assets_helper(ret,n,room_or_rooms,completed);
                var found = val.found;
                ret = val.ret;
                if(found){
                    f1 = true;
                }
            }
            if(f1){
                return false;
            }
        });
    }
    return ret;
}

//helper function for get_available_rooms_assets
function get_available_rooms_assets_helper(ret,room_index_in_sequence_object,room,completed,multiroom){
    multiroom = typeof multiroom !== 'undefined' ? multiroom : false;
    var found = false;
    var temp = {};
    $.each(room,function(room_name,asset_arr){
        //I am looping here but this will really just be a one step loop that will access the index
        //and the value in the room. Index would be something like "room_1" and value would be the 
        //the array of assets.
        var f1 = false;
        temp[room_name] = [];
        $.each(asset_arr,function(i,asset){
            //finally loop through asset array
            var f2 = false;
            if(!multiroom){
                if(typeof ret[room_index_in_sequence_object] == 'undefined'){
                    ret[room_index_in_sequence_object] = {};
                }
                if(typeof ret[room_index_in_sequence_object][room_name] == 'undefined'){
                    ret[room_index_in_sequence_object][room_name] = [];
                }
            }
            if($.isArray(asset)){ 
                //asset_name is expected to be a string, BUT, if it is an array, then it means that 
                //multiple assets are available to the user at the same time.
                //so I have to loop through each one to see if user already completed all of them
                if(!multiroom){
                    ret[room_index_in_sequence_object][room_name].push(asset);
                }
                else{
                    temp[room_name].push(asset);
                }
                $.each(asset,function(x,asset_name){
                    if(!is_asset_completed(asset_name,completed)){
                        f2 = true;
                        found = true;
                        return false;;
                    }
                });
            }
            else{
                //see if the user completed the asset
                if(!multiroom){
                    ret[room_index_in_sequence_object][room_name].push(asset);
                }
                else{
                    temp[room_name].push(asset);
                }
                if(!is_asset_completed(asset,completed)){
                    found = true;
                    f2 = true;
                }
            }
            if(f2){
                f1 = true;
                return false;
            }
        });
        if(multiroom){
            ret = temp;
        }
        if(f1){
            return false;
        }
    });
    //if it reaches this point, that means the user completed the assets in this room.
    //I am going to return false if room is completed, this way it can just continue on.
    return {ret:ret,found:found};  
}

//The get_available_rooms_assets and the helepr fucntionality is just rediculous but I was in a time crunch,
//so I just threw this together. Basically this is supposed to go through the sequence.json file and identify where
//the user is based on what the completed cookie has. It is supposed to return only the items that were completed, 
//or are available for the user to click (next up). Basically, this returns the same sequence with all unavailable
//assets/rooms removed. As the cookie grows, so does what this returns.
//returns an array of assets that are available
get_available_rooms_assets = function(){
    var completed = getCookie('completed');
    var ret = [];
    if($.isArray(sequence[0])){
        $.each(sequence[0],function(room_i,room_obj){
            $.each(room_obj,function(room_name,asset_arr){
                var temp = {};
                temp[room_name] = [asset_arr[0]];
                ret.push(temp);
            });
        });
    }
    else{
        $.each(sequence[0],function(room_name,asset_arr){
            var temp = {};
            temp[room_name] = [asset_arr[0]];
            ret.push(temp);
        });
    }
    if(completed !== null){
        ret = [];
        completed = completed.split('^');
        //loop through the sequence json object and figure out what point the user is at by
        //comparing the sequence to the completed cookie array.
        //as soon as an asset is reached that is not in the completed array, the loop is broken,
        //and that point in the sequence is returned.
        //I return room number and the next available asset(s).
        $.each(sequence,function(n,room_or_rooms){
            var f1 = false;
            if($.isArray(room_or_rooms)){
                //this means multiple rooms are available together because in json multiple rooms in one array
                var onefound = false;
                var temp = [];
                $.each(room_or_rooms,function(x,room){
                    //so I have to loop through each room
                    var val = get_available_rooms_assets_helper([],n,room,completed,true);
                    var found = val.found;
                    temp.push(val.ret);
                    if(found){
                        onefound = true;
                    }
                });
                ret.push(temp);
                if(onefound){
                    f1 = true;
                }
            }
            else{
                var val = get_available_rooms_assets_helper(ret,n,room_or_rooms,completed);
                var found = val.found;
                ret = val.ret;
                if(found){
                    f1 = true;
                }
            }
            if(f1){
                return false;
            }
        });
    }
    return ret;
}

//making functions for check if room is allowed and check if asset is allowed.
check_room_asset_allowed = function(room_num,asset_num){
    asset_num = typeof asset_num !== 'undefined' ? asset_num : false;
    //if((room_num == 0 || room_num == 1) || (room_num == 2 && (asset_num == 1 || asset_num == 0))){
    if(room_num == 0){
        return true;
    }
    //ok so first lets get available sequence.
    var available_sequence = get_available_rooms_assets();
    //available sequence should be an array of objects, however, if an array is present instead of an object, then
    //it means that that this array holds multiple objects/rooms that are available together at the same time.
    var allowed = false;
    $.each(available_sequence,function(i,room_or_rooms){
        var f1 = false;
        if($.isArray(room_or_rooms)){
            $.each(room_or_rooms,function(room_index,one_room){
                var f2 = false;
                $.each(one_room,function(room_name,assets){
                    var available_room_num = parseInt(room_name.slice(-1));
                    if(available_room_num == room_num){
                        allowed = true;
                        f2 = true;
                        return false;
                    }
                });
                if(f2){
                    f1 = true;
                    return false;
                }
            });
        }
        else{
            $.each(room_or_rooms,function(room_name,assets){
                var available_room_num = parseInt(room_name.slice(-1));
                if(asset_num !== false){
                    $.each(assets,function(asset_index,asset){
                        var f2 = false;
                        //means we want to check the asset, loop further through the assets.
                        if($.isArray(asset)){
                            $.each(asset,function(index,asset_name){
                                var available_asset_num = parseInt(asset_name.slice(-1));
                                if(room_num == available_room_num && asset_num <= available_asset_num){
                                    allowed = true;
                                    f2 = true;
                                    return false;
                                }
                            });
                        }
                        else{
                            var available_asset_num = parseInt(asset.slice(-1));
                            if(room_num == available_room_num && asset_num <= available_asset_num){
                                allowed = true;
                                f2 = true;
                            }
                        }
                        if(f2){
                            f1 = true;
                            return false;
                        }
                    });
                }
                else{
                    if(available_room_num == room_num){
                        allowed = true;
                        f1 = true;
                        return false;
                    }
                }
            });
        }
        if(f1){
            return false;
        }
    });
    return allowed;
}

//open doors function runs after every asset has been completed. This function opens doors, and also checks if lessons
//have been completed within the module to congratulate the user if they have completed either or both lessons
open_doors = function(){
    //if(is_room_completed(1)){   //complete
        // $('.world .room_3').addClass('highlight');
        // $('#main-menu .room_1').parent().removeClass('open');
        // $('#main-menu .room_3').parent().addClass('open');
        // if(getCookie('lesson_1_done') === null){
        //     setCookie('lesson_1_done','true');
        //     //lets do a pop up saying that room 2 was complete
        //     setTimeout(function(){
        //         $('.lightbox-content').html('<div class="confirm-outer-wrap"><div class="okay-overlay"></div><div class="confirm-inner-table"><div class="confirm-inner-cell"><div class="confirm-inner">You have completed Lesson 1. You are now able to move on to Lesson 2 or review Lesson 1 again.<br/><div class="btn okay"><div class="btn-child">Continue</div></div></div></div></div></div>');
        //         lightbox_show();
        //         $('.lightbox-content').on(device_click_event,'.btn.okay',function(){
        //             close_lightbox();
        //             this.removeEventListener('ended', arguments.callee);
        //         });
        //     },500);           
        // }
        // else{
        //     $('.world, #main-menu').off(device_click_event, '.room_3');
        //     $('.world, #main-menu').on(device_click_event, '.room_3', function(e) {
        //         if(good_click(e) && !$(e.target).hasClass('open-arrow')){
        //             enter_room(3);
        //         }
        //     });
        // }
        //$('body').addClass('finished');
    //}
}

//removes all divs from the world.
function remove_all_nodes(object){
    /* optimal javascript remove method does not unbind listeners
    var i = object.childNodes.length;
    while(i--){
        object.removeChild(object.lastChild);
    }
    */
    //if (object != null && object.value == '') {
        var done = [];
        $(object).children().each(function(){
            if($(this).is('script')){
                $(this).remove();
            }
            else{
                var classList = $(this).attr('class').split(/\s+/);
                $.each(classList, function(i, v){
                    if(done.indexOf(v) == -1 && 
                        v !== 'plane' && 
                        v !== 'highlight' && 
                        v !== 'asset' && 
                        v !== 'slides-wrap'){
                            //here i turn off the listeneres because they were stacking.
                            //it looks like initially i would NOT turn off events on items that were room_assets_
                            //but since I changed how I do things, I am going to turn off listeneres on assets also.
                            //if(v.indexOf('_asset_') == -1 && v.indexOf('room_') == -1){
                                if(v !== 'room_0' && v !== 'room_1' && v !== 'room_3'){   //i dont want to turn off doors
                                    $('.'+v).off(device_click_event);
                                    $('.world').off(device_click_event,'.'+v);
                                }
                            //}
                            done.push(v);
                    }
                });
                $(this).remove();
                classList = null;
            }
        });
    //}
}

//not used, was previously used to build 3d cubes/boxes
// function buildCube( room, colour, w, h, d, x, y, z, rx, ry, rz ) {
//     //builds a cube/block out of 6 planes.
//     room.add( new Plane(colour, h, w, x, y, z, 0, 180, 90));       
//     room.add( new Plane(colour, w, d, x, y, z, 90, 0, 0));
//     room.add( new Plane(colour, d, h, x, y, z, 0, 270, 0));
//     room.add( new Plane(colour, d, h, x+w, y, z+d, 0, 90, 0));
//     room.add( new Plane(colour, w, d, x+w, y+h, z, 90, 180, 0));
//     room.add( new Plane(colour, w, h, x, y, z+d, 0, 0, 0));
// }
function show_slide(selector,start,end){
    //fade function that fades in a selected dive for 300 seconds then fades it out.
    setTimeout(function(){
       $(selector).fadeIn(300);
    },start);
    setTimeout(function(){
        $(selector).fadeOut(300);
    },end);
}
//checks if the click is good (had issue with two finger clicks on my touchscreen laptop).
//this checks that its single finger touch
function good_click(e){
    //checks to make sure a touch event wasnt a two finger touch event. 
    //most events in the program are one finger touch so thich checker makes sure that this is the case.
    if(e.type == 'touchend' || e.type == 'touchstart'){
        if(typeof e.originalEvent !== 'undefined'){
            if(e.originalEvent.touches.length <= 1){
                return true;
            }
        }
        else if(typeof e.isTrigger !== 'undefined'){
            return true;
        }
    }
    else if(e.type != 'touchend' && e.type != 'touchstart'){
        return true;
    }
    return false;
}
/*#################COOKIE FUNCTIONS######################*/
//delete single cookie
function deleteCookie(cname){
    //deletes cookie name
    document.cookie = cname + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
//set
function setCookie(cname, cvalue) {
    //sets cookie name and val
    //console.log('set cookie: '+cname+' and '+ cvalue);
    var v = cvalue;
    if(cname.indexOf('rotation') > -1 || cname.indexOf('position') > -1){
        v = cvalue | 0;
    }
    var d = new Date();
    d.setTime(d.getTime() + (365*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + v + "; " + expires;
}
//get
function getCookie(name) {
    //gets cookie by name
    function escape(s) { return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1'); };
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    var ret = match ? match[1] : null;
    //if positon, rotation or room, then return as int.
    if(ret !== null && (name.indexOf('position') > -1 || name.indexOf('rotation') >-1 || name.indexOf('room') >-1)){
        ret = parseInt(ret);
    }
    return ret;
}
//deletes all cookies
function deleteAllCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}
/*#################ENDS COOKIE FUNCTIONS######################*/


//AUDIO FUNCTIONS
//play audio takes in the file name, and callback. If callback is present, then it is fired after
//the audio is completed playing.
play_audio = function(src, callback){
    callback = typeof callback !== 'undefined' ?  callback : false;
    if (callback!=false) {
        callback();
    }
    return true;
    /*main_audio.pause();
    //main_audio.src='/audio/'+src+'.mp3';
    main_audio.src='/audio/sound1.mp3';
    main_audio.load();
    setTimeout(function(){main_audio.play();},200);
    if(callback!==false){
        $(main_audio).off();
        $(main_audio).on('ended', function(){
            callback();
            this.removeEventListener('ended', arguments.callee);
        });
    }*/
}
//stops audio from playing.
stop_audio = function(){
    main_audio.pause();
}